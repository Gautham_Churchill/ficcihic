<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

	return view('auth.login');
});

Auth::routes();

Route::match(['get','post'],'/register-user','UserController@registerUser');

Route::match(['get','post'],'/login-user','UserController@loginUser');

Route::get('/login/google', 'Auth\LoginController@redirectToGoogle');

Route::get('login/google/callback', 'Auth\LoginController@handleGoogleCallback');

Route::get('/login/facebook', 'Auth\LoginController@redirectToFacebook');
Route::get('/login/facebook/callback', '	Auth\LoginController@handleFacebookCallback');

Route::get('/login/linkedin', 'Auth\LoginController@redirectToLinkedin');
Route::get('/login/linkedin/callback', '	Auth\LoginController@handleLinkedinCallback');

Route::match(['get','post'],'/forgot-code','UserController@forgotCode');
Route::match(['get','post'],'/paymentUser','UserController@paymentUser');
Route::match(['get','post'],'/offlinepayment/{id}','UserController@offlinepayment');
Route::get('/success_payment', 'UserController@success_payment');
Route::get('/ticket/{email}/{name}', 'UserController@ticket');
Route::get('/payment_failed', 'UserController@payment_failed');
Route::get('/offline', 'UserController@offline');
Route::get('/forgot', 'UserController@forgot');
Route::get('/lobby', 'LobbyController@index');
Route::get('/exhibition', 'LobbyController@exhibition');
Route::get('/entry', 'LobbyController@enter');
Route::get('/auditorium', 'LobbyController@auditorium');
Route::get('/countdown', 'LobbyController@countdown');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/download/{email}/{name}', 'UserController@download');
Route::get('/testmail', 'UserController@testmail');