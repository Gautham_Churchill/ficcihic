<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'Resp_Code', 'TXnID', 'Invoice_no', 'Amount',
    ];
}
