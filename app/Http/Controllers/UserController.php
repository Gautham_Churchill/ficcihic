<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use RealRashid\SweetAlert\Facades\Alert;
use App\User;
use App\Payment;
use Auth;
use Mail;
use Validator;
use Image;
use DB;
use Redirect;
use PDF;

class UserController extends Controller
{
    public function registerUser(Request $request){


      $rules = array(

            'ContactName'=>'required',
            'Designation'=>'required',
            'OfficialEmail'=>'required|email',
            /*'image' => 'required|image|mimes:jpeg,jpg',*/
            'mobile'=>'required|max:10|min:10',
            'phone'=>'required|max:10|min:10',
            'terms'=>'required',
            'Organisation'=>'required',
            'CompanyAddress'=>'required',
            'payment_type'=>'required',
            'payment_amount'=>'required',
        );

      $params = $request->all();
       $validator = Validator::make($params, $rules);

     if ($validator->fails()) {
        return response()->json(['status' => 'errors', 'errors' => array()]);
            $request->merge(array('add_form_validate' => 1));
            //print_r($request->all());die('jjj');
            $input['add_form_validate'] = '1';
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        else{

          if($request->isMethod('post')){

            $data=$request->all();

            // echo "<pre>";print_r($data);die();

            if($request->hasfile('image')){

                $file=$request->file('image');
                $filename='image'.time().'.'.$request->image->extension();
                $destination=storage_path('../public/upload');
                $file->move($destination,$filename);
                $path="/".$filename;
             }

            $userCount=User::where('email',$data['OfficialEmail'])->count();

            if($userCount>0){
                  return response()->json(['status' => 'error', 'error' => 'Email Already Exist']);
                  return redirect()->back()->with('flash_message_error','Email Already Exist');
            }
            else{
              if(isset($data['ficci_no'])){
                $ficci_no = $data['ficci_no'];
              }else{
                $ficci_no = '';
              }

              if(isset($data['GSTNumber'])){
                $GSTNumber = $data['GSTNumber'];
              }else{
                $GSTNumber = '';
              }
              //echo "<pre>"; print_R($_POST);  die;
              if(isset($path)){
                $pathurl = $path;
              }else{
                $pathurl = '';
              }

                  $user=new User;

                  $user->first_name=$data['ContactName'];
                  $user->password='FICCI-HIC-2020';           
                  $user->desgination=$data['Designation'];
                  $user->email=$data['OfficialEmail'];
                  $user->mobile=$data['mobile'];
                  $user->linkedin_link=$data['linkedin_link'];
                  $user->twitter_link=$data['twitter_link'];
                  $user->terms=$data['terms'];
                  $user->name_of_organisation=$data['Organisation'];
                  $user->address=$data['CompanyAddress'];
                  $user->state=$data['State'];
                  $user->zipcode=$data['ZipCode'];
                  $user->phone=$data['phone'];
                  $user->ficci_member=$data['ficci_member'];
                  $user->ficci_no=$ficci_no;
                  $user->gst=$data['gst'];
                  $user->gst_no=$GSTNumber;
                  $user->payment_type=$data['payment_type'];
                  $user->payment_amount=$data['payment_amount'];
                  $user->image=$pathurl;

                  $save=$user->save();

                  if($save) 
                  {
                    /*$edata = [
                               'name' => $request->first_name,
                               'Message' => 'Just registered!',
                            ];
                    event(new LoginNotification($edata));*/
                      return response()->json(['status' => 'success', 'id'=> $user->id]);
                  }
                  else
                  {
                      return response()->json(['status' => 'error']);
                  }

                  $first_name = $request->first_name;
                  $password=$user->password;
                  $email = $request->email;


                  $demo = array(
                          'first_name' => $request->first_name,
                            'password'=>$password,
                              'email' => $request->email
                        );


                  Mail::send('mails.send_mail',$demo,function($message)use($first_name,$password,$email){
                    $message->to($email)->subject('FICCI Event Code');
        });
                  return redirect('/login')->with('flash_message_success','You are successfully registered. Event code sended to your email');
                  
            }
      }
        }

    	
    	
    }

    public function loginUser(Request $request){

        
        if($request->isMethod('post')){

          $user =User::where('email',$request->email)->get()->first();
        // echo "<pre>";print_r($user);die();

        $count_email=DB::table('users')->where(['email'=>$request->email])->count();

        $count_password=DB::table('users')->where(['password'=>$request->password])->count();

        if($count_email==0){

          return redirect()->back()->with('flash_message_error','Your Email and Password does not match');
        }

        else{

          $email=$user->email;
          $password=$user->password;
          $image=$user->image;
          $first_name=$user->first_name;
          $data = [
            'name' => $first_name,
            'Message' => 'Just joined the Event!',
          ];
          event(new LoginNotification($data));

          if($email==$request->email && $password==$request->password)
          {
            return redirect('/countdown');
          }
          else{

            return redirect()->back()->with('flash_message_error','Your Email and Password does not match');
          }

        }

          
        }
    }

    public function forgot()
    {

        return view('auth.forgot');
    }

    public function ticket($email, $name)
    {

        return view('ticket', array('email' => $email, 'first_name' => $name));
    }

    public function offline()
    {
        $user = User::where('id', session()->get('id'))->first();

        $first_name = $user->first_name;
        $email = $user->email;
        $desgination = $user->desgination;
        $name_of_organisation = $user->name_of_organisation;
        $address = $user->address;
        $gst_no = $user->gst_no;
        $id = $user->id;
        
        if ($user->payment_amount == 'ficci') {
            $amount= 3000;
        }
        else if ($user->payment_amount == 'nonficci') {
            $amount= 3500;
        }
        else if ($user->payment_amount == 'academia') {
            $amount= 750;
        }
         $demo = array(
            'Organisation' => $name_of_organisation,
            'CompanyAddress' => $address,
            'ContactName' => $first_name,
            'Designation' => $desgination,
            'OfficialEmail' => $email,
            'GSTNumber' => $gst_no,
            'RegId' => $id,
            'Amount' => $amount,
            'pageurl' => 'http://ficcihic.optimizevents.com/paymentUser'
          );
        Mail::send('mails.neft_rtgs',$demo,function($message)use($first_name, $email){
          $message->to($email)->subject('Pending Payment Approval');
        });
        return view('auth.offline', ['data' => $demo]);
    }

    public function forgotCode(Request $request){

      // $user=$request->all();

      // echo "<pre>";print_r($user);die();

      $user=User::where('email',$request->email)->first();


      $count_email=DB::table('users')->where(['email'=>$request->email])->count();

      if($count_email==0){

        return redirect()->back()->with('flash_message_error','This email does not exist');
      }

      else{

        $password=$user->password;
        $email=$user->email;

        $demo = array(
                            'first_name='=>$user->first_name,
                            'password'=>$user->password,
                              'email' => $user->email
                        );

       Mail::send('mails.send_forgotMail',$demo,function($message)use($password,$email){
                    $message->to($email)->subject('FICCI Event Code');
        });

       return redirect('/login')->with('flash_message_success','Your Event Code is sended to your mail');


      }

      


      // echo "<pre>";print_r($user_password);die();

    }

    public function paymentUser(Request $request)
    {

      if($request->isMethod('post')){
        $payment=new Payment;
        $user = User::where('id', $_POST['Regid'])->first();
        $payment->Resp_Code=$_POST['Resp_Code'];       
        $payment->TXnID=$_POST['TXnID'];
        $payment->Invoice_no=$_POST['Invoice_no'];
        $payment->Regid=$_POST['Regid'];
        $payment->Amount=$_POST['Amount'];
        $save=$payment->save();
        if($save){
          
          $first_name = $user->first_name;
          if( $_POST['Resp_Code'] == '100'){
            return redirect()->to('/success_payment')->with( [ 'id' => $_POST['Regid'], 'first_name' => $user->first_name ] );
          }else{
            return redirect()->to('/payment_failed')->with( [ 'id' => $_POST['Regid'], 'first_name' => $user->first_name ] );
           
          }
          //echo $this->payment_code($_POST['Resp_Code']);
        }
      }

    }

    public function payment_failed()
    {   
      
        $user = User::where('id', session()->get('id'))->first();

        $first_name = $user->first_name;
        $email = $user->email;
        $desgination = $user->desgination;
        $name_of_organisation = $user->name_of_organisation;
        $address = $user->address;
        $gst_no = $user->gst_no;
        $id = $user->id;
        
        if ($user->payment_amount == 'ficci') {
            $amount= 3000;
        }
        else if ($user->payment_amount == 'nonficci') {
            $amount= 3500;
        }
        else if ($user->payment_amount == 'academia') {
            $amount= 750;
        }
         $demo = array(
            'Organisation' => $name_of_organisation,
            'CompanyAddress' => $address,
            'ContactName' => $first_name,
            'Designation' => $desgination,
            'OfficialEmail' => $email,
            'GSTNumber' => $gst_no,
            'RegId' => $id,
            'Amount' => $amount,
            'pageurl' => 'http://ficcihic.optimizevents.com/paymentUser'
          );
        Mail::send('mails.payment_cancel',$demo,function($message)use($first_name, $email){
          $message->to($email)->subject('Payment Cancellation');
        });
        return view('auth.payment_failed', [ 'id' => $user->id,'first_name' => $user->first_name]);
    }

     public function success_payment()
    {
        $user = User::where('id', session()->get('id'))->first();
        $first_name = $user->first_name;
        $email = $user->email;
         $demo = array(
            'first_name' => $first_name,
            'email' => $email
          );
        Mail::send('mails.success_payment',$demo,function($message)use($first_name, $email){
          $message->to($email)->subject('Successfully Payment');
        });
        return view('auth.success_payment', [ 'id' => session()->get('id'),'first_name' => session()->get('first_name')]);
    }

    public function offlinepayment(Request $request, $id)
    {

      $user = User::where('id', $id)->first();
      $first_name = $user->first_name;
      $email = $user->email;
      $desgination = $user->desgination;
      $name_of_organisation = $user->name_of_organisation;
      $address = $user->address;
      $gst_no = $user->gst_no;
      $id = $user->id;
      
      if ($user->payment_amount == 'ficci') {
          $amount= 3000;
      }
      else if ($user->payment_amount == 'nonficci') {
          $amount= 3500;
      }
      else if ($user->payment_amount == 'academia') {
          $amount= 750;
      }
       $demo = array(
          'Organisation' => $name_of_organisation,
          'CompanyAddress' => $address,
          'ContactName' => $first_name,
          'Designation' => $desgination,
          'OfficialEmail' => $email,
          'GSTNumber' => $gst_no,
          'RegId' => $id,
          'Amount' => $amount,
          'pageurl' => 'http://ficcihic.optimizevents.com/paymentUser'
        );
      
      return redirect()->to('/offline')->with( [ 'id' => $id, 'first_name' => $first_name ] );
      //return view('auth.offline', [ 'data' => $demo ]);
    }

    public function download($email, $name) {
      $pdf = PDF::loadView('ticket', array('email' => $email, 'first_name' => $name));

      // download PDF file with download method
      return $pdf->download('pdf_file.pdf');
    }

    public function payment_code($tid){

      $rc=array(
        'E000' =>'"Received Confirmation from the Bank, Yet to Settle the transaction with the Bank, Settlement Pending.', 
        'E001' =>'Unauthorized Payment Mode', 
        'E002' =>'Unauthorized Key', 
        'E003' =>'Unauthorized Packet', 
        'E004' =>'Unauthorized Merchant', 
        'E005' =>'Unauthorized Return URL', 
        'E006' =>'"Transaction Already Paid, Received Confirmation from the Bank, Yet to Settle the transaction with the Bank', 
        'E007' =>'Transaction Failed', 
        'E008' =>'Failure from Third Party due to Technical Error', 
        'E009' =>'Bill Already Expired', 
        'E0031' =>'Mandatory fields coming from merchant are empty', 
        'E0032' =>'Mandatory fields coming from database are empty', 
        'E0033' =>'Payment mode coming from merchant is empty', 
        'E0034' =>'PG Reference number coming from merchant is empty', 
        'E0035' =>'Sub merchant id coming from merchant is empty', 
        'E0036' =>'Transaction amount coming from merchant is empty', 
        'E0037' =>'Payment mode coming from merchant is other than 0 to 9', 
        'E0038' =>'Transaction amount coming from merchant is more than 9 digit length', 
        'E0039' =>'Mandatory value Email in wrong format', 
        'E00310' =>'Mandatory value mobile number in wrong format', 
        'E00311' =>'Mandatory value amount in wrong format', 
        'E00312' =>'Mandatory value Pan card in wrong format', 
        'E00313' =>'Mandatory value Date in wrong format', 
        'E00314' =>'Mandatory value String in wrong format', 
        'E00315' =>'Optional value Email in wrong format', 
        'E00316' =>'Optional value mobile number in wrong format', 
        'E00317' =>'Optional value amount in wrong format', 
        'E00318' =>'Optional value pan card number in wrong format', 
        'E00319' =>'Optional value date in wrong format', 
        'E00320' =>'Optional value string in wrong format', 
        'E00321' =>'Request packet mandatory columns is not equal to mandatory columns set in enrolment or optional columns are not equal to optional columns length set in enrolment', 
        'E00322' =>'Reference Number Blank', 
        'E00323' =>'Mandatory Columns are Blank', 
        'E00324' =>'Merchant Reference Number and Mandatory Columns are Blank', 
        'E00325' =>'Merchant Reference Number Duplicate', 
        'E00326' =>'Sub merchant id coming from merchant is non numeric', 
        'E00327' =>'Cash Challan Generated', 
        'E00328' =>'Cheque Challan Generated', 
        'E00329' =>'NEFT Challan Generated', 
        'E00330' =>'Transaction Amount and Mandatory Transaction Amount mismatch in Request URL', 
        'E00331' =>'UPI Transaction Initiated Please Accept or Reject the Transaction', 
        'E00332' =>'Challan Already Generated, Please re-initiate with unique reference number', 
        'E00333' =>'Referer value is null / invalid Referer', 
        'E00334' =>'Value of Mandatory parameter Reference No and Request Reference No are not matched', 
        'E0801' =>'FAIL', 
        'E0802' =>'User Dropped', 
        'E0803' =>'Canceled by user', 
        'E0804' =>'User Request arrived but card brand not supported', 
        'E0805' =>'Checkout page rendered Card function not supported', 
        'E0806' =>'Forwarded / Exceeds withdrawal amount limit', 
        'E0807' =>'PG Fwd Fail / Issuer Authentication Server failure', 
        'E0808' =>'Session expiry / Failed Initiate Check, Card BIN not present', 
        'E0809' =>'Reversed / Expired Card', 
        'E0810' =>'Unable to Authorize', 
        'E0811' =>'Invalid Response Code or Guide received from Issuer', 
        'E0812' =>'Do not honor', 
        'E0813' =>'Invalid transaction', 
        'E0814' =>'Not Matched with the entered amount', 
        'E0815' =>'Not sufficient funds', 
        'E0816' =>'No Match with the card number', 
        'E0817' =>'General Error', 
        'E0818' =>'Suspected fraud', 
        'E0819' =>'User Inactive', 
        'E0820' =>'ECI 1 and ECI6 Error for Debit Cards and Credit Cards', 
        'E0821' =>'ECI 7 for Debit Cards and Credit Cards', 
        'E0822' =>'System error. Could not process transaction', 
        'E0823' =>'Invalid 3D Secure values', 
        'E0824' =>'Bad Track Data', 
        'E0825' =>'Transaction not permitted to cardholder', 
        'E0826' =>'Rupay timeout from issuing bank', 
        'E0827' =>'OCEAN for Debit Cards and Credit Cards', 
        'E0828' =>'E-commerce decline', 
        'E0829' =>'This transaction is already in process or already processed', 
        'E0830' =>'Issuer or switch is inoperative', 
        'E0831' =>'Exceeds withdrawal frequency limit', 
        'E0832' =>'Restricted card', 
        'E0833' =>'Lost card', 
        'E0834' =>'Communication Error with NPCI', 
        'E0835' =>'The order already exists in the database', 
        'E0836' =>'General Error Rejected by NPCI', 
        'E0837' =>'Invalid credit card number', 
        'E0838' =>'Invalid amount', 
        'E0839' =>'Duplicate Data Posted', 
        'E0840' =>'Format error', 
        'E0841' =>'SYSTEM ERROR', 
        'E0842' =>'Invalid expiration date', 
        'E0843' =>'Session expired for this transaction', 
        'E0844' =>'FRAUD - Purchase limit exceeded', 
        'E0845' =>'Verification decline', 
        'E0846' =>'Compliance error code for issuer', 
        'E0847' =>'Caught ERROR of type:[ System.Xml.XmlException ] . strXML is not a valid XML string', 
        'E0848' =>'Incorrect personal identification number', 
        'E0849' =>'Stolen card', 
        'E0850' =>'Transaction timed out, please retry', 
        'E0851' =>'Failed in Authorize - PE', 
        'E0852' =>'Cardholder did not return from Rupay', 
        'E0853' =>'Missing Mandatory Field(s)The field card_number has exceeded the maximum length of', 
        'E0854' =>'Exception in CheckEnrollmentStatus: Data at the root level is invalid. Line 1, position 1.', 
        'E0855' =>'CAF status = 0 or 9', 
        'E0856' =>'412', 
        'E0857' =>'Allowable number of PIN tries exceeded', 
        'E0858' =>'No such issuer', 
        'E0859' =>'Invalid Data Posted', 
        'E0860' =>'PREVIOUSLY AUTHORIZED', 
        'E0861' =>'Cardholder did not return from ACS', 
        'E0862' =>'Duplicate transmission', 
        'E0863' =>'Wrong transaction state', 
        'E0864' =>'Card acceptor contact acquirer');
        return $rc[$tid];
    }


    public function testmail()
    {

        $user = User::where('id', 1)->first();

        $first_name = $user->first_name;
        $email = $user->email;
        $desgination = $user->desgination;
        $name_of_organisation = $user->name_of_organisation;
        $address = $user->address;
        $gst_no = $user->gst_no;
        $id = $user->id;
        
        if ($user->payment_amount == 'ficci') {
            $amount= 3000;
        }
        else if ($user->payment_amount == 'nonficci') {
            $amount= 3500;
        }
        else if ($user->payment_amount == 'academia') {
            $amount= 750;
        }
         $demo = array(
            'Organisation' => $name_of_organisation,
            'CompanyAddress' => $address,
            'ContactName' => $first_name,
            'Designation' => $desgination,
            'OfficialEmail' => $email,
            'GSTNumber' => $gst_no,
            'RegId' => $id,
            'Amount' => $amount,
            'pageurl' => 'http://ficcihic.optimizevents.com/paymentUser'
          );
        Mail::send('mails.neft_rtgs',$demo,function($message)use($first_name, $email){
          $message->to($email)->subject('Pending Payment Approval');
        });
    }
}
