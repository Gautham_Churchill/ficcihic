<?php

namespace App\Http\Controllers;

use App\Lobby;
use Illuminate\Http\Request;
use DB;

class LobbyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user=DB::table('users')->get();
        return view('lobby',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function enter()
    {
        return view('enter');
    }

    public function auditorium()
    {
        return view('auditorium');
    }

    public function exhibition()
    {
        return view('exhibition');
    }

    public function countdown()
    {
        return view('auth.countdown');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lobby  $lobby
     * @return \Illuminate\Http\Response
     */
    public function show(Lobby $lobby)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lobby  $lobby
     * @return \Illuminate\Http\Response
     */
    public function edit(Lobby $lobby)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lobby  $lobby
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lobby $lobby)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lobby  $lobby
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lobby $lobby)
    {
        //
    }
}
