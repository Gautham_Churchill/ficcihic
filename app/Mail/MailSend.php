<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailSend extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $subject = EMAIL_VERIFICATION_SUBJECT;
                $date = date('d.m.Y');
                Mail::send('mail.verify', ['name' => $name, 'email' => $email, 'code' => $code, 'date' => $date], function ($message)
                    use($email, $subject, $name) {
                        $message->to($email, $name);
                        $message->subject($subject);
                });
                
                if( count(Mail::failures()) > 0 ) {
                    loggerhelper('Send mail fail ', 'alert', ['email' => $email]);
                } else {
                    loggerhelper('Send mail success ', 'info', ['email' => $email, 'code' => $code]);
                    
                }
        // return $this->view('view.name');
    }
}
