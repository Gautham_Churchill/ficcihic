@extends('layouts.lobby')

@section('content')
<body>
<div class="entry_gate_bg">
<a class="leftbaricon" id="sidebarCollapse"><img src="img/info.png" width="22" /></a> 
<a href="#" class="logo"></a>
<a href="/lobby" class="entergate"></a>

<div class="right-iconbar">
<a href="#" class="profile"><div class="profile_btn_green"></div></a>
<a href="#" class="right-iconbar-ml5"><img src="img/share.png" width="24" /></a> 
<a href="#" class="right-iconbar-ml5"><img src="img/speak.png" width="24" /></a> 
<a href="#" class="right-iconbar-ml5"><img src="img/fullscreen.png" width="24" /></a> 
<a href="#" class="right-iconbar-nobot right-iconbar-ml5"><img src="img/setting.png" width="24" /></a>
</div>
<!-- Sidebar  -->
<nav id="sidebar">
  <div id="dismiss"><img src="img/close.png" width="22" /></div>
  <div class="sidebar-header">
    <h3>FICCI VIRTUAL HIC 2020</h3>
  </div>
  <div class="ban"><img src="img/inside-home-banner.jpg" /></div>
  <div class="padding16">
  <div class="w3-bar">
  <button class="w3-bar-item w3-button" onClick="openCity('abouttab')">About</button>
  <button class="w3-bar-item w3-button" onClick="openCity('speakertab')">Speaker</button>
  <button class="w3-bar-item w3-button" onClick="openCity('sponsorstab')">Sponsors</button>
  <button class="w3-bar-item w3-button" onClick="openCity('contacttab')">Contact</button>
</div>
<div id="abouttab" class="w3-container city">
  
  <p align="justify">Health  insurance in India is the fastest growing segment and projected to grow at a  CAGR of 21% till 2025. The growth will be fuelled by both demand and supply.  Demand is driven by a growing middle class, young insurable population and  increasing awareness of the need for protection. Supply is driven by the  increase in products, insurers and intermediaries operating in this space. &nbsp; There are,  however, gaps in the areas of digitisation, financing, technology and fraud  control that hold the industry back.&nbsp; Addressing these gaps through  innovative ideas can remove all the roadblocks to penetration and continuing  rapid growth. Keeping  these imperatives in mind, the conference discussions this year will focus on  bridging the identified industry gaps through innovation. The outcome of this  effort will be an increase in <strong style="font-weight:700;">TRUST, AFFORDABILITY &amp; ACCESSIBILITY</strong>.  The conference will also showcase a curated set of start-ups that are working  to address these gaps with solutions that could potentially be game changers.</p>
</div>
<div id="speakertab" class="w3-container city" style="display:none">
<div class="owl-carousel owl-theme">

<div class="item">
<div class="speaker_main">
<img src="img/speakers/G-Srinivasan.png" />
<h4>Mr G. Srinivasan</h4>
<p>Advisor, FICCI Health
Insurance Committee & Director, National
Insurance Academy</p>
</div>

<div class="speaker_main">
<img src="img/speakers/Suresh-Mathur.png" />
<h4>Mr Suresh Mathur</h4>
<p>Executive Director IRDAI</p>
</div>
</div>

<div class="item">
<div class="speaker_main">
<img src="img/speakers/Kunnel-Prem.png" />
<h4>Mr Kunnel Prem</h4>
<p>Chief Executive Officer
Insurance Information Bureau of India</p>
</div>
<div class="speaker_main">
<img src="img/speakers/D-V-S-Ramesh.png" />
<h4>Mr D V S Ramesh</h4>
<p>General Manager - Health IRDAI</p>
</div>
</div>

<div class="item">
<div class="speaker_main">
<img src="img/speakers/Girish-Rao.png" />
<h4>Mr Girish Rao</h4>
<p>Immd. Past Chair, FICCI Health Insurance Committee; Chairman
& Managing Director, Vidal Healthcare</p>
</div>
<div class="speaker_main">
<img src="img/speakers/Malti-Jaswal.png" />
<h4>Ms Malti Jaswal</h4>
<p>Advisor<br />
National Health Authority (NHA) GoI</p>
</div>
</div>

<div class="item">
<div class="speaker_main">
<img src="img/speakers/Nandakumar-Jairam.png" />
<h4>Dr Nandakumar Jairam</h4>
<p>Advisor, FICCI Health Insurance Committee & Chairman & CEO, Columbia Asia Hospitals</p>
</div>
<div class="speaker_main">
<img src="img/speakers/Mayank-Bathwal.png" />
<h4>Mr Mayank Bathwal</h4>
<p>Co-Chair, FICCI Health Insurance Committee, MD & CEO, Aditya Birla Health Insurance</p>
</div>
</div>

<div class="item">
<div class="speaker_main">
<img src="img/speakers/Ritesh-Kumar.png" />
<h4>Mr Ritesh Kumar</h4>
<p>Co-chair, FICCI Health
Insurance Committee
MD & CEO, HDFC Ergo General Insurance Company </p>
</div>
<div class="speaker_main">
<img src="img/speakers/Nandakumar-Jairam.png" />
<h4>Mr Prasun Sikdar</h4>
<p><a href="#">Co-Chair, FICCI Health Insurance Committee
MD & CEO, ManipalCigna Health Insurance</a></p>
</div>
</div>
</div>
</div>
<div id="sponsorstab" class="w3-container city" style="display:none">
<div class="past_sponsors">
<img src="img/sponsors.jpg" />
</div>
</div>

<div id="contacttab" class="w3-container city" style="display:none">
 <div class="contact_main">
 <h4>Mr. Harsh Vardhan</h4>
 <h5>Senior Assistant Director</h5>
 <p><img src="img/contact.png" width="16" />  9810808766</p>
 <p><img src="img/mail-icon.png" width="16" /> <a href="#">harsh.vardhan@ficci.com</a></p>
 </div>
 <div class="contact_main">
 <h4>Mr. Kapil Chadha</h4>
 <h5>Research Associate</h5>
 <p><img src="img/contact.png" width="16" />  9654631600</p>
 <p><img src="img/mail-icon.png" width="16" /> <a href="#">kapil.chadha@ficci.com</a></p>
 </div>
 
 <div class="contact_main">
 <h4>Ms. Beena Mulani</h4>
 <h5>Executive Officer</h5>
 <p><img src="img/contact.png" width="16" />  7428323420</p>
 <p><img src="img/mail-icon.png" width="16" /> <a href="#">beena.mulani@ficci.com</a></p>
 </div>
</div>
  
</div>
</nav>


</div>

@endsection
