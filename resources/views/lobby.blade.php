@extends('layouts.lobby')

@section('content')
<body>
<div class="lobby_bg">
<a class="leftbaricon" id="sidebarCollapse"><img src="img/info.png" width="22" /></a> 
<a href="#" class="logo"></a>
<div class="top_bar">
<a href="#" class="top_bar_logo">FICCI HIC 2020</a>
<!--<div class="top_bar_search">
<div class="search">
      <input type="text" class="searchTerm" placeholder="Search">
      <button type="submit" class="searchButton">
        <i class="fa fa-search"></i>
     </button>
   </div>
</div>-->
<a href="#" class="top_bar_attending" data-toggle="modal" data-target="#myModal2">
<p><span>Now Attending</span> : 0.3k</p>
</a>
<a href="#" class="top_bar_attending" data-toggle="modal" data-target="#myModal2">
<p><span>This Location</span> : 1.2k</p>
</a>
<a href="#" class="top_bar_agenda" data-toggle="modal" data-target="#modal-agenda"><img src="img/calendar-icon.png" width="24" /></a>
<!--<a href="#" class=""></a>-->
      <div id="dd" class="wrapper-dropdown-3 top_bar_agenda" tabindex="1"><img src="img/notifiction-icon.png" width="17" />
        <ul class="dropdown">
          <li><a href="#">
          <div class="top-baar-notification">
          <div class="user_avatar"><img src="img/atul-sahai.png"></div>
          <div class="comment_body">
          <p>Lorem Ipsum is simply dummy text of the printing. </p>
          </div>
          </div>
          </a></li>
          <li><a href="#">
          <div class="top-baar-notification">
          <div class="user_avatar"><img src="img/atul-sahai.png"></div>
          <div class="comment_body">
          <p>Lorem Ipsum is simply dummy text of the printing. </p>
          </div>
          </div>
          </a></li>
          <li><a href="#">
          <div class="top-baar-notification">
          <div class="user_avatar"><img src="img/atul-sahai.png"></div>
          <div class="comment_body">
          <p>Lorem Ipsum is simply dummy text of the printing. </p>
          </div>
          </div>
          </a></li>
        </ul>
      </div>
<!--<div class="dropdown">
<a href="#" class="top_bar_drop"><img src="img/topbar-arow.png" width="10" /></a>
<div class="dropdown-content">
    <a href="#"><img src="img/conversation-icon.png" width="18" /> Public group chat</a>
    <a href="#"><img src="img/trophy-icon.png" width="18" /> Leadership board</a>
  </div>
</div>-->
</div>


<a href="/auditorium" class="auditorium"></a>
<a href="#" class="networking_lounge"></a>
<a href="/exhibition" class="exhibition_hall"></a>








<div class="right-iconbar"> 
<a href="#" class="profile"><div class="profile_btn_green"></div></a>
<a href="#" class="right-iconbar-ml5"><img src="img/share.png" width="24" /></a> 
<a href="#" class="right-iconbar-ml5"><img src="img/speak.png" width="24" /></a> 
<a href="#" class="right-iconbar-ml5"><img src="img/fullscreen.png" width="24" /></a> 
<a href="#" class="right-iconbar-nobot right-iconbar-ml5"><img src="img/setting.png" width="24" /></a> 
</div>
<div class="menu-bottom"> <a href="/lobby" class="menu-icon"><img src="img/lobby.png" width="36" />
  <p>Lobby</p>
  </a> <a href="#" class="menu-icon"><img src="img/falcon1hall.png" width="36" />
  <p>Falcon 1 Hall</p>
  </a> <a href="/exhibition" class="menu-icon"><img src="img/exhibition.png" width="36" />
  <p>Exhibition</p>
  </a> <!--<a href="#" class="menu-icon"><img src="img/irish-hall.png" width="36" />
  <p>Irish Hall</p>
  </a>--> <a href="#" class="menu-icon"><img src="img/helpdesk.png" width="36" />
  <p>Helpdesk</p>
  </a> <a href="#" class="menu-icon" data-toggle="modal" data-target="#modal-exhibition-tweetwall"><img src="img/social.png" width="36" />
  <p>Social</p>
  </a> <a href="#" class="menu-icon" data-toggle="modal" data-target="#modal-exhibition-feedback"><img src="img/feedback.png" width="36" />
  <p>Feedback</p>
  </a>
  <a href="#" class="menu-icon" data-toggle="modal" data-target="#modal-souvenir-booklet"><img src="img/souvenir-booklet.png" width="36" />
  <p>Souvenir Booklet</p></a>
  <div class="dropup" style="float:right; width:6px;"> 
  <a href="#" class="menu-icon" style="margin-top:-5px;"><img src="img/dots.png" width="6" style="margin-top:10px;" /></a> 
  <div class="dropup-content">
    <!--<a href="#"><img src="img/briefcase-icon.png" width="18" />  Briefcase</a>
    <a href="#"><img src="img/feedback-icon.png" width="18" />  Feedback</a>-->
    <a href="#"><img src="img/helpdesk-icon.png" width="18" />  Helpdesk</a>
    <a href="#" data-toggle="modal" data-target="#myModal2"><img src="img/attendee-icon.png" width="18" />  Attedee List</a>
  </div>
  </div>
  </div>

<!-- Sidebar  -->
<nav id="sidebar">
  <div id="dismiss"><img src="img/close.png" width="22" /></div>
  <div class="sidebar-header">
    <h3>FICCI VIRTUAL HIC 2020</h3>
  </div>
  <div class="ban"><img src="img/inside-home-banner.jpg" /></div>
  <div class="padding16">
  <div class="w3-bar">
  <button class="w3-bar-item w3-button" onClick="openCity('abouttab')">About</button>
  <button class="w3-bar-item w3-button" onClick="openCity('speakertab')">Speaker</button>
  <button class="w3-bar-item w3-button" onClick="openCity('sponsorstab')">Sponsors</button>
  <button class="w3-bar-item w3-button" onClick="openCity('contacttab')">Contact</button>
</div>
<div id="abouttab" class="w3-container city">
<p align="justify">Health  insurance in India is the fastest growing segment and projected to grow at a  CAGR of 21% till 2025. The growth will be fuelled by both demand and supply.  Demand is driven by a growing middle class, young insurable population and  increasing awareness of the need for protection. Supply is driven by the  increase in products, insurers and intermediaries operating in this space. &nbsp; There are,  however, gaps in the areas of digitisation, financing, technology and fraud  control that hold the industry back.&nbsp; Addressing these gaps through  innovative ideas can remove all the roadblocks to penetration and continuing  rapid growth. Keeping  these imperatives in mind, the conference discussions this year will focus on  bridging the identified industry gaps through innovation. The outcome of this  effort will be an increase in <strong style="font-weight:700;">TRUST, AFFORDABILITY &amp; ACCESSIBILITY</strong>.  The conference will also showcase a curated set of start-ups that are working  to address these gaps with solutions that could potentially be game changers.</p>
</div>
<div id="speakertab" class="w3-container city" style="display:none">
<div class="owl-carousel owl-theme">
<div class="item">
<div class="speaker_main">
<img src="img/speakers/G-Srinivasan.png" />
<h4>Mr G. Srinivasan</h4>
<p>Advisor, FICCI Health
Insurance Committee & Director, National
Insurance Academy</p>
</div>
<div class="speaker_main">
<img src="img/speakers/Suresh-Mathur.png" />
<h4>Mr Suresh Mathur</h4>
<p>Executive Director IRDAI</p>
</div>
</div>
<div class="item">
<div class="speaker_main">
<img src="img/speakers/Kunnel-Prem.png" />
<h4>Mr Kunnel Prem</h4>
<p>Chief Executive Officer
Insurance Information Bureau of India</p>
</div>
<div class="speaker_main">
<img src="img/speakers/D-V-S-Ramesh.png" />
<h4>Mr D V S Ramesh</h4>
<p>General Manager - Health IRDAI</p>
</div>
</div>
<div class="item">
<div class="speaker_main">
<img src="img/speakers/Girish-Rao.png" />
<h4>Mr Girish Rao</h4>
<p>Immd. Past Chair, FICCI Health Insurance Committee; Chairman
& Managing Director, Vidal Healthcare</p>
</div>
<div class="speaker_main">
<img src="img/speakers/Malti-Jaswal.png" />
<h4>Ms Malti Jaswal</h4>
<p>Advisor<br />
National Health Authority (NHA) GoI</p>
</div>
</div>
<div class="item">
<div class="speaker_main">
<img src="img/speakers/Nandakumar-Jairam.png" />
<h4>Dr Nandakumar Jairam</h4>
<p>Advisor, FICCI Health Insurance Committee & Chairman & CEO, Columbia Asia Hospitals</p>
</div>
<div class="speaker_main">
<img src="img/speakers/Mayank-Bathwal.png" />
<h4>Mr Mayank Bathwal</h4>
<p>Co-Chair, FICCI Health Insurance Committee, MD & CEO, Aditya Birla Health Insurance</p>
</div>
</div>
<div class="item">
<div class="speaker_main">
<img src="img/speakers/Ritesh-Kumar.png" />
<h4>Mr Ritesh Kumar</h4>
<p>Co-chair, FICCI Health
Insurance Committee
MD & CEO, HDFC Ergo General Insurance Company </p>
</div>
<div class="speaker_main">
<img src="img/speakers/Nandakumar-Jairam.png" />
<h4>Mr Prasun Sikdar</h4>
<p><a href="#">Co-Chair, FICCI Health Insurance Committee
MD & CEO, ManipalCigna Health Insurance</a></p>
</div>
</div>
</div>
</div>
<div id="sponsorstab" class="w3-container city" style="display:none">
<div class="past_sponsors">
<img src="img/sponsors.jpg" />
</div>
</div>
<div id="contacttab" class="w3-container city" style="display:none">
 <div class="contact_main">
 <h4>Mr. Harsh Vardhan</h4>
 <h5>Senior Assistant Director</h5>
 <p><img src="img/contact.png" width="16" />  9810808766</p>
 <p><img src="img/mail-icon.png" width="16" /> <a href="#">harsh.vardhan@ficci.com</a></p>
 </div>
 <div class="contact_main">
 <h4>Mr. Kapil Chadha</h4>
 <h5>Research Associate</h5>
 <p><img src="img/contact.png" width="16" />  9654631600</p>
 <p><img src="img/mail-icon.png" width="16" /> <a href="#">kapil.chadha@ficci.com</a></p>
 </div>
 <div class="contact_main">
 <h4>Ms. Beena Mulani</h4>
 <h5>Executive Officer</h5>
 <p><img src="img/contact.png" width="16" />  7428323420</p>
 <p><img src="img/mail-icon.png" width="16" /> <a href="#">beena.mulani@ficci.com</a></p>
 </div>
</div>
</div>
</nav>


<!-- Popups -->
<!-- Agenda -->
<div class="modal fade" id="modal-agenda" role="dialog">
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
        
        <div class="w3-bar">
  <button class="w3-bar-item w3-button agenda-tab-lineh w_color" onClick="openCity('agenda-monday-tab')"><span style="display:block; font-weight:600; font-size:18px;text-align: left;">19 AUGUST</span> Wednesday</button>
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
  <div id="agenda-monday-tab" class="w3-container city">
  <embed height="500" src="pdf/hic-agenda.pdf" style="width:100%;">
  </div>
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
<!-- profile pop-->  
<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
        <div class="container scrollbar scrollbar-primary">
        <div class="row">
  <button type="button" class="close pro-close" data-dismiss="modal">&times;</button>
    <div class="col-lg-1 col-sm-12">Sort</div>
    <div class="col-lg-4 col-sm-6">
    <select class="pro-select">
    <option value="0">All</option>
    
  </select>
    </div>
    <div class="col-lg-4 col-sm-6">
    <div class="search">
      <input type="text" class="searchTerm" placeholder="Search">
      <button type="submit" class="searchButton">
        <i class="fa fa-search"></i>
     </button>
   </div>
    </div>
    
    <div class="pro-bor-bot"></div>
    <div class="clear"></div>
    <div class="col-lg-4 col-sm-6">
      <div class="card hovercard">
        <div class="cardheader"> </div>
        <div class="info">
        <div class="avatar"> <img alt="" src="http://lorempixel.com/100/100/people/9/"> </div>
          <div class="title"> <a target="_blank" href="https://scripteden.com/">Script Eden</a> </div>
          <div class="desc">Passionate designer</div>
          <div class="desc">Curious developer</div>
          <div class="desc">Tech geek</div>
        <div class="bottom"> 
        <a class="" href="#" style="margin-right:5px;"><img src="img/in.png" width="18" /></a> 
        <a class="" href="#" style="margin-right:5px;"><img src="img/tw.png" width="20" /></a> 
        <a class="" href="#"><img src="img/chat-icon.png" width="22" /></a> 
        </div>
        </div>
        
      </div>
    </div>
    <div class="col-lg-4 col-sm-6">
      <div class="card hovercard">
        <div class="cardheader"> </div>
        <div class="info">
        <div class="avatar"> <img alt="" src="http://lorempixel.com/100/100/people/9/"> </div>
          <div class="title"> <a target="_blank" href="https://scripteden.com/">Script Eden</a> </div>
          <div class="desc">Passionate designer</div>
          <div class="desc">Curious developer</div>
          <div class="desc">Tech geek</div>
        <div class="bottom"> 
        <a class="" href="#" style="margin-right:5px;"><img src="img/in.png" width="18" /></a> 
        <a class="" href="#" style="margin-right:5px;"><img src="img/tw.png" width="20" /></a> 
        <a class="" href="#"><img src="img/chat-icon.png" width="22" /></a> 
        </div>
        </div>
        
      </div>
    </div>
    <div class="col-lg-4 col-sm-6">
      <div class="card hovercard">
        <div class="cardheader"> </div>
        <div class="info">
        <div class="avatar"> <img alt="" src="http://lorempixel.com/100/100/people/9/"> </div>
          <div class="title"> <a target="_blank" href="https://scripteden.com/">Script Eden</a> </div>
          <div class="desc">Passionate designer</div>
          <div class="desc">Curious developer</div>
          <div class="desc">Tech geek</div>
        <div class="bottom"> 
        <a class="" href="#" style="margin-right:5px;"><img src="img/in.png" width="18" /></a> 
        <a class="" href="#" style="margin-right:5px;"><img src="img/tw.png" width="20" /></a> 
        <a class="" href="#"><img src="img/chat-icon.png" width="22" /></a> 
        </div>
        </div>
        
      </div>
    </div>
    
    <div class="col-lg-4 col-sm-6">
      <div class="card hovercard">
        <div class="cardheader"> </div>
        <div class="info">
        <div class="avatar"> <img alt="" src="http://lorempixel.com/100/100/people/9/"> </div>
          <div class="title"> <a target="_blank" href="https://scripteden.com/">Script Eden</a> </div>
          <div class="desc">Passionate designer</div>
          <div class="desc">Curious developer</div>
          <div class="desc">Tech geek</div>
        <div class="bottom"> 
        <a class="" href="#" style="margin-right:5px;"><img src="img/in.png" width="18" /></a> 
        <a class="" href="#" style="margin-right:5px;"><img src="img/tw.png" width="20" /></a> 
        <a class="" href="#"><img src="img/chat-icon.png" width="22" /></a> 
        </div>
        </div>
        
      </div>
    </div>
    <div class="col-lg-4 col-sm-6">
      <div class="card hovercard">
        <div class="cardheader"> </div>
        <div class="info">
        <div class="avatar"> <img alt="" src="http://lorempixel.com/100/100/people/9/"> </div>
          <div class="title"> <a target="_blank" href="https://scripteden.com/">Script Eden</a> </div>
          <div class="desc">Passionate designer</div>
          <div class="desc">Curious developer</div>
          <div class="desc">Tech geek</div>
        <div class="bottom"> 
        <a class="" href="#" style="margin-right:5px;"><img src="img/in.png" width="18" /></a> 
        <a class="" href="#" style="margin-right:5px;"><img src="img/tw.png" width="20" /></a> 
        <a class="" href="#"><img src="img/chat-icon.png" width="22" /></a> 
        </div>
        </div>
        
      </div>
    </div>
    <div class="col-lg-4 col-sm-6">
      <div class="card hovercard">
        <div class="cardheader"> </div>
        <div class="info">
        <div class="avatar"> <img alt="" src="http://lorempixel.com/100/100/people/9/"> </div>
          <div class="title"> <a target="_blank" href="https://scripteden.com/">Script Eden</a> </div>
          <div class="desc">Passionate designer</div>
          <div class="desc">Curious developer</div>
          <div class="desc">Tech geek</div>
        <div class="bottom"> 
        <a class="" href="#" style="margin-right:5px;"><img src="img/in.png" width="18" /></a> 
        <a class="" href="#" style="margin-right:5px;"><img src="img/tw.png" width="20" /></a> 
        <a class="" href="#"><img src="img/chat-icon.png" width="22" /></a> 
        </div>
        </div>
        
      </div>
    </div>
    
    <div class="col-lg-4 col-sm-6">
      <div class="card hovercard">
        <div class="cardheader"> </div>
        <div class="info">
        <div class="avatar"> <img alt="" src="http://lorempixel.com/100/100/people/9/"> </div>
          <div class="title"> <a target="_blank" href="https://scripteden.com/">Script Eden</a> </div>
          <div class="desc">Passionate designer</div>
          <div class="desc">Curious developer</div>
          <div class="desc">Tech geek</div>
        <div class="bottom"> 
        <a class="" href="#" style="margin-right:5px;"><img src="img/in.png" width="18" /></a> 
        <a class="" href="#" style="margin-right:5px;"><img src="img/tw.png" width="20" /></a> 
        <a class="" href="#"><img src="img/chat-icon.png" width="22" /></a> 
        </div>
        </div>
        
      </div>
    </div>
    
  </div>
        </div>
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
<!-- feedback pop-->  
<div class="modal fade" id="modal-exhibition-feedback" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close pro-close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mb-0 mt-0 pt-0 pb-0 center exhibition_video_pop_head">Feedback</h4>
      </div>
        <div class="modal-body">
        <div class="container">
        <form>
        <div class="row">
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Name</label>
        <div class="col-lg-6 col-sm-6"><p class="request_a_quote_pop_head">R K Malhotra</p></div>
        <div class="clear"></div>
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Contact Us</label>
        <div class="col-lg-6 col-sm-6"><p class="request_a_quote_pop_head">rajesh.malhotra@rishirajmedia.com</p></div>
        <div class="clear"></div>
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Mobile No.*</label>
        <div class="col-lg-6 col-sm-6"><p class="request_a_quote_pop_head">+91 9810077205</p></div>
        <div class="clear"></div>
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Subject</label>
        <div class="col-lg-8 col-sm-6 mb-3"><input type="text" name="subject" value="" required="" class="form-control"></div>
        <div class="clear"></div>
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Message</label>
        <div class="col-lg-8 col-sm-6 mb-3"><textarea name="message" class="form-control" required="" style="height:100px;"></textarea></div>
        <div class="clear"></div>
        <div class="col-lg-3 col-sm-6"></div>
        <div class="col-lg-8 col-sm-6" style="text-align:right;"><input type="submit" value="Send" class="btn btn-danger"></div>
        </div>
        </form>
        </div>
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
<!-- tweet wall pop-->  
<div class="modal fade" id="modal-exhibition-tweetwall" role="dialog">
    <div class="modal-dialog modal-xlg">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close pro-close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mb-0 mt-0 pt-0 pb-0 center exhibition_video_pop_head">Tweet wall</h4>
      </div>
        <div class="modal-body">
        <div class="tweet_wall">
          <img src="img/tweet-wall.jpg">
          </div>
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div> 
<!-- Souvenir Booklet -->
<div class="modal fade" id="modal-souvenir-booklet" role="dialog">
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close pro-close" data-dismiss="modal">&times;</button>
      </div>
        <div class="modal-body">
        <embed height="500" src="pdf/HIC-Souvenir-Booklet.pdf" style="width:100%;">
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
<!-- video on load-->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close pro-close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div class="exhibition_video_pop">
        <iframe height="360" src="img/ficci-hic-2020.mp4?wmode=opaque&amp;autoplay=1&amp;rel=0&amp;color=white" frameborder="0" allow="autoplay; fullscreen; picture-in-picture; xr-spatial-tracking; encrypted-media" allowfullscreen=""></iframe>
        <a href="#" data-dismiss="modal">Skip the video</a>
        </div>
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
    </div>
  </div> 
<!-- Popups End-->

</div>

@foreach($user as $users)
  
  <?php 
    $responseinJSON = json_encode($users);
    //echo $responseinJSON;
    ?>

@endforeach



@endsection
