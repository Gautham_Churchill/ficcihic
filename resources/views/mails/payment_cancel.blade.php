<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Optimize</title>
<style>
.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
}
thead {
    display: table-header-group;
    vertical-align: middle;
    border-color: inherit;
}
.table>thead>tr>th {
    vertical-align: bottom;
    
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border: 1px solid #ddd;
}
th {
    text-align: left;
}
</style>
</head>

<body>
<table align="center" width="600" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #dedcdc;padding:20px;">
  <tr>
    <td><img src="http://ficcihic.optimizevents.com/img/optimize-logo.png" /></td>
  </tr>
  <tr>
    <td>
    <p style="font-family: Verdana, Geneva, sans-serif;"><b>Hi {{$ContactName}}</b></p>
    <p style="font-family: Verdana, Geneva, sans-serif;">You just tried to make an online payment on our platform, but encountered some error.</p>
    <p style="font-family: Verdana, Geneva, sans-serif;">Your details have been registered with us but to experience the Virtual HIC 2020 you will have to complete the Payment Process.</p>
    <p style="font-family: Verdana, Geneva, sans-serif;">You can easily make the online payment <form action="http://payment.ficci.com/fvhico/paymentprocess.asp" method="post" >

   <input type="text" name="Organisation" value="{{$Organisation}}" style="display: none;" hidden="">
   <input type="text" name="CompanyAddress" value="{{$CompanyAddress}}" style="display: none;" hidden="">
   <input type="text" name="ContactName" value="{{$ContactName}}" style="display: none;" hidden="">
   <input type="text" name="Designation" value="{{$Designation}}" style="display: none;" hidden="">
   <input type="text" name="City" value="" style="display: none;" hidden="">
   <input type="text" name="ZipCode" value="" style="display: none;" hidden="">
   <input type="text" name="State" value="" style="display: none;" hidden="">
   <input type="text" name="country" value="" style="display: none;" hidden="">
   <input type="text" name="OfficialEmail" value="{{$OfficialEmail}}" style="display: none;" hidden="">
   <input type="text" name="GSTNumber" value="{{$GSTNumber}}" style="display: none;" hidden="">
   <input type="text" name="RegId" value="{{$RegId}}" style="display: none;" hidden="">
   <input type="text" name="Amount" value="{{$Amount}}" style="display: none;" hidden="">
   <input type="text" name="quantity" value="1" style="display: none;" hidden="">
   <input type="text" name="pageurl" value="{{$pageurl}}" style="display: none;" hidden="">
    <input type="submit" name="" value="payment">
</form></p>
    <p style="font-family: Verdana, Geneva, sans-serif;">Or you can even Transfer the payment through NEFT/RTGS</p>
    <p style="font-family: Verdana, Geneva, sans-serif;">
    <table class="table-responsive table table-bordered">
    <thead>
    <th style="font-family: Verdana, Geneva, sans-serif;">NEFT/ RTGS/ IMPS</th>
    </thead>
   <tbody>
   <tr>
   <td>
   <p style="font-family: Verdana, Geneva, sans-serif;">
  <b>Bank Name</b> – Yes Bank<br />

<b>Branch</b> - 56 Janpath, Alps Building, Connaught Place, New Delhi 110001<br />
<b>A/C No.</b> – 013694600000041<br />
<b>A/C Holder</b> – Federation of Indian Chambers of Commerce and Industry<br />

<b>A/C type</b> – Saving<br />

<b>IFSC Code</b> – YESB0000136
   </p>
   </td>
   </tr>
   </tbody>
    </table>
    </p>
    <p style="font-family: Verdana, Geneva, sans-serif;">Team Optimize</p>
    <p></p>
    <p></p>
    </td>
  </tr>
</table>

</body>
</html>
