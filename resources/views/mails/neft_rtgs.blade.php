<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Optimize</title>
<style>
.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
}
thead {
    display: table-header-group;
    vertical-align: middle;
    border-color: inherit;
}
.table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
    border-top: 0;
}
.table>thead>tr>th {
    vertical-align: bottom;
    border-bottom: 2px solid #ddd;
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd;
}
th {
    text-align: left;
}
</style>
</head>

<body>
<table align="center" width="800" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #dedcdc;padding:20px;">
  <tr>
    <td><img src="http://ficcihic.optimizevents.com/img/optimize-logo.png" /></td>
  </tr>
  <tr>
    <td>
    <h3 style="font-family: Verdana, Geneva, sans-serif;">All good things comes to those who wait…</h3>
    <p style="font-family: Verdana, Geneva, sans-serif;">We've sent a request to confirm your payment through the NEFT/RTGS to FICCI. As soon as the same is confirmed your account will be Activated.</p>
    <p style="font-family: Verdana, Geneva, sans-serif;">Note: You'll be notified through an email once the request is approved.</p>
    <p style="font-family: Verdana, Geneva, sans-serif;">Alternatively, you can make the payment through Online Payment method <form action="http://payment.ficci.com/fvhico/paymentprocess.asp" method="post" >

   <input type="text" name="Organisation" value="{{$Organisation}}" style="display: none;" hidden="">
   <input type="text" name="CompanyAddress" value="{{$CompanyAddress}}" style="display: none;" hidden="">
   <input type="text" name="ContactName" value="{{$ContactName}}" style="display: none;" hidden="">
   <input type="text" name="Designation" value="{{$Designation}}" style="display: none;" hidden="">
   <input type="text" name="City" value="" style="display: none;" hidden="">
   <input type="text" name="ZipCode" value="" style="display: none;" hidden="">
   <input type="text" name="State" value="" style="display: none;" hidden="">
   <input type="text" name="country" value="" style="display: none;" hidden="">
   <input type="text" name="OfficialEmail" value="{{$OfficialEmail}}" style="display: none;" hidden="">
   <input type="text" name="GSTNumber" value="{{$GSTNumber}}" style="display: none;" hidden="">
   <input type="text" name="RegId" value="{{$RegId}}" style="display: none;" hidden="">
   <input type="text" name="Amount" value="{{$Amount}}" style="display: none;" hidden="">
   <input type="text" name="quantity" value="1" style="display: none;" hidden="">
   <input type="text" name="pageurl" value="{{$pageurl}}" style="display: none;" hidden="">
    <input type="submit" name="" value="payment">
</form></p>
    <p style="font-family: Verdana, Geneva, sans-serif;">Team Optimize</p>
    <p></p>
    <p></p>
    </td>
  </tr>
</table>

</body>
</html>
