<!DOCTYPE HTML>
<html lang="en-US">
<head>
<title>Optimize by Enseur</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description" content="Template by CocoBasic" />
<meta name="keywords" content="HTML, CSS, JavaScript, PHP" />
<meta name="author" content="CocoBasic" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}" />
<link href='http://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800%7CPlayfair%20Display:700%7CPT%20Serif:400i' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css"  href="{{ asset('style.css') }}" />
<link rel="stylesheet" type="text/css"  href="{{ asset('exhibition_html/style.css') }}" />
<link rel="stylesheet" href="{{ asset('tab.css') }}">
<link rel="stylesheet" href="{{ asset('bootstrap/bootstrap.min.css') }}">
<!-- notification -->
<link rel="stylesheet" href="{{ asset('/notification/style.css') }}">
<!-- Owl Stylesheets -->
<link rel="stylesheet" href="{{ asset('assets/owlcarousel/assets/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/owlcarousel/assets/owl.theme.default.min.css') }}">
<!--[if lt IE 9]>
                <script src="js/html5shiv.js"></script>            
                <script src="js/respond.min.js"></script>                   
        <![endif]-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.fa {
  padding: 6px;
  font-size: 14px;
  width: 26px;
  height:26px;
  text-align: center;
  text-decoration: none;
  margin: 2px 0px; margin-right:4px;
  border-radius:50%;
}

.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-google {
  background: #dd4b39;
  color: white;
}

.fa-linkedin {
  background: #007bb5;
  color: white;
}

.fa-youtube {
  background: #bb0000;
  color: white;
}

.fa-instagram {
  background: #125688;
  color: white;
}

.fa-pinterest {
  background: #cb2027;
  color: white;
}

.fa-snapchat-ghost {
  background: #fffc00;
  color: white;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

.fa-skype {
  background: #00aff0;
  color: white;
}

.fa-android {
  background: #a4c639;
  color: white;
}

.fa-dribbble {
  background: #ea4c89;
  color: white;
}

.fa-vimeo {
  background: #45bbff;
  color: white;
}

.fa-tumblr {
  background: #2c4762;
  color: white;
}

.fa-vine {
  background: #00b489;
  color: white;
}

.fa-foursquare {
  background: #45bbff;
  color: white;
}

.fa-stumbleupon {
  background: #eb4924;
  color: white;
}

.fa-flickr {
  background: #f40083;
  color: white;
}

.fa-yahoo {
  background: #430297;
  color: white;
}

.fa-soundcloud {
  background: #ff5500;
  color: white;
}

.fa-reddit {
  background: #ff5700;
  color: white;
}

.fa-rss {
  background: #ff6600;
  color: white;
}
</style>
</head>




@yield('content')


<!--Load JavaScript-->
<!-- jQuery CDN - Slim version (=without AJAX) --> 
<script src="{{ asset('bootstrap/jquery-3.3.1.slim.min.js') }}"></script>
<!-- notification drop -->
<script type="text/javascript">
			
			function DropDown(el) {
				this.dd = el;
				this.placeholder = this.dd.children('span');
				this.opts = this.dd.find('ul.dropdown > li');
				this.val = '';
				this.index = -1;
				this.initEvents();
			}
			DropDown.prototype = {
				initEvents : function() {
					var obj = this;

					obj.dd.on('click', function(event){
						$(this).toggleClass('active');
						return false;
					});

					obj.opts.on('click',function(){
						var opt = $(this);
						obj.val = opt.text();
						obj.index = opt.index();
						obj.placeholder.text(obj.val);
					});
				},
				getValue : function() {
					return this.val;
				},
				getIndex : function() {
					return this.index;
				}
			}

			$(function() {

				var dd = new DropDown( $('#dd') );

				$(document).click(function() {
					// all dropdowns
					$('.wrapper-dropdown-3').removeClass('active');
				});

			});

		</script>  
<!-- tab --> 
<script>
function openCity(cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  document.getElementById(cityName).style.display = "block";  
}
</script>
<!-- tab end -->
<!-- Popper.JS --> 
<script src="{{ asset('bootstrap/popper.min.js') }}"></script> 
<!-- Bootstrap JS --> 
<script src="{{ asset('bootstrap/bootstrap.min.js') }}"></script>
<!-- owlcarousel in the tab -->
<script src="{{ asset('assets/owlcarousel/owl.carousel.js') }}"></script>
<script>
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:20,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
})
</script>
 
<script src="{{ asset('exhibition_html/js/jquery.js') }}"></script>
<script src="{{ asset('exhibition_html/js/jquery.smartmenus.min.js') }}"></script> 
<script src="{{ asset('exhibition_html/js/jquery.prettyPhoto.js') }}"></script> 
<script src="{{ asset('exhibition_html/js/jquery.sticky-kit.min.js') }}"></script>
<script src="{{ asset('exhibition_html/js/imagesloaded.pkgd.js') }}"></script> 
<script src="{{ asset('exhibition_html/js/jquery.fitvids.js') }}"></script>
<script src="{{ asset('exhibition_html/js/tipper.js') }}"></script>
<script src="{{ asset('exhibition_html/js/swiper.min.js') }}"></script>
<script src="{{ asset('exhibition_html/js/main.js') }}"></script>
</body>
</html>