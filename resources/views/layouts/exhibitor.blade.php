<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Ficci virtual health insurance conference 2020') }}</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Wilio Survey, Quotation, Review and Register form Wizard by Ansonika.">
    <meta name="author" content="Ansonika">

    <!-- Favicons-->
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
    <!--<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">-->
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/menu.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendors.css') }}" rel="stylesheet">

    
    <!-- <script src="{{ asset('js/app.js') }}" defer></script>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>
<body>
    <!-- <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <ul class="navbar-nav ml-auto">
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4"> -->
            @yield('content')

            @include('sweetalert::alert')

        <!-- </main>
    </div> -->
    <!-- COMMON SCRIPTS --> 
    <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script> 
    <script src="{{ asset('js/common_scripts.min.js') }}"></script> 
    <script src="{{ asset('js/velocity.min.js') }}"></script> 
    <script src="{{ asset('js/functions.js') }}"></script> 
    <script src="{{ asset('js/pw_strenght.js') }}"></script> 


    <!-- Wizard script --> 
    <script src="{{ asset('js/registration_func.js') }}"></script>

    <!-- Sweet Alert -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
      $(function() {

         $("#fname_error_message").hide();
         $("#lname_error_message").hide();
         $("#designation_error_message").hide();
         $("#email_error_message").hide();
         $("#mobile_error_message").hide();

         $("#org_error_message").hide();
         $("#address_error_message").hide();
        

         var error_fname = false;
         var error_lname = false;
         var error_designation = false;
         var error_email = false;
         var error_mobile = false;
         var error_org=false;
         var error_address=false;
        
         

         $("#form_fname").focusout(function(){
            check_fname();
         });
         /* $("#form_lname").focusout(function() {
            check_sname();
         });*/
          $("#form_designation").focusout(function() {
            check_fname();
            check_designation();
         });
         $("#form_email").focusout(function() {
            check_fname();
            check_designation();
            check_email();
         });
         $("#form_mobile").focusout(function() {
            check_fname();
            check_designation();
            check_email();
            check_mobile();
         });

         $("#form_org").focusout(function() {
            check_fname();
            check_designation();
            check_email();
            check_mobile();
            check_org();
         });
         $("#form_address").focusout(function() {
            check_fname();
            check_designation();
            check_email();
            check_mobile();
            check_org();
            check_address();
         });

         $("#phone").focusout(function() {
            check_fname();
            check_designation();
            check_email();
            check_mobile();
            check_org();
            check_address();
            check_phone();
         });
        

         function check_fname() {
            var pattern = /^[a-zA-Z_ ]*$/;
            var fname = $("#form_fname").val();
            if (pattern.test(fname) && fname !== '') {
               $("#fname_error_message").hide();
               
            } else if (fname == '') {
                $("#fname_error_message").html("*Please enter your name*");
                $("#fname_error_message").show();
                return false;
                error_fname = true;
               
            } else {
               $("#fname_error_message").html("*Should contain only Characters*");
               $("#fname_error_message").show();
               return false;
               error_fname = true;
            }
         }

         function check_sname() {
            
            var pattern = /^[a-zA-Z]*$/;
            var sname = $("#form_lname").val()
            if (pattern.test(sname) && sname !== '') {
               $("#lname_error_message").hide();
               
            } else {
                $("#form_lname").val('');
               $("#lname_error_message").html("*Should contain only Characters*");
               $("#lname_error_message").show();
               return false;
               error_fname = true;
            }
         }


         function check_designation() {
          
            var sname = $("#form_designation").val()
            if (sname == '') {
                $("#designation_error_message").html("*Please enter your designation*");
                $("#designation_error_message").show();
                return false;
                error_fname = true;
               
            }else{
              $("#designation_error_message").hide();
            }
         }


          function check_email() {
            
            var pattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            var email = $("#form_email").val();
            if (pattern.test(email) && email !== '') {
               $("#email_error_message").hide();
               
            } else if (email == '') {
                $("#email_error_message").html("*Please enter your email*");
                $("#email_error_message").show();
                return false;
                error_fname = true;
               
            }  else {
               $("#email_error_message").html("*Invalid Email*");
               $("#email_error_message").show();
               return false;
               error_email = true;
            }
         }


         function check_org() {
          
            var pattern = /^[a-zA-Z]*$/;
            var sname = $("#form_org").val()
            if (pattern.test(sname) && sname !== '') {
               $("#org_error_message").hide();
               
            } else if (sname == '') {
                $("#org_error_message").html("*Please enter your Organisation*");
                $("#org_error_message").show();
                return false;
                error_fname = true;
               
            }
         }

         function check_address() {
          
            var pattern = /^[a-z0-9\\-]+$/;
            var sname = $("#form_address").val()
            if (pattern.test(sname) && sname !== '') {
               $("#address_error_message").hide();
               
            } else if (sname == '') {
                $("#address_error_message").html("*Please enter your address*");
                $("#address_error_message").show();
                return false;
                error_fname = true;
               
            }
         }


         function check_mobile() {

          var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
            var phoneNumber = $("#form_mobile").val()
            if (filter.test(phoneNumber)) {
              if(phoneNumber.length==10){
                   var validate = true;
              } else {
                  $("#mobile_error_message").html("*Please put 10  digit mobile number*");
                  $("#form_mobile").val('');
                  $("#mobile_error_message").show();
                  var validate = false;
              }
            } else if (phoneNumber == '') {
                $("#mobile_error_message").html("*Please enter your mobile number*");
                $("#mobile_error_message").show();
                $("#form_mobile").val('');
                return false;
                validate = true;
               
            } 
            else {
              $("#form_mobile").val('');
              $("#mobile_error_message").html("*Not a valid mobile number*");
              $("#mobile_error_message").show();
              var validate = false;
            }
         
            if(validate){
              $("#mobile_error_message").hide();
            }
         }

         function check_phone() {
          var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
            var phoneNumber = $("#phone").val()
            if (filter.test(phoneNumber)) {
              if(phoneNumber.length==10){
                   var validate = true;
              } else {
                  $("#phone_error_message").html("*Please put 10  digit mobile number*");
                  $("#phone").val('');
                  $("#phone_error_message").show();
                  var validate = false;
              }
            } else if (phoneNumber == '') {
                $("#phone_error_message").html("*Please enter your mobile number*");
                $("#phone_error_message").show();
                $("#phone").val('');
                return false;
                validate = true;
               
            } 
            else {
              $("#phone").val('');
              $("#phone_error_message").html("*Not a valid mobile number*");
              $("#phone_error_message").show();
              var validate = false;
            }
         
            if(validate){
              $("#phone_error_message").hide();
            }
         }

       
      });

$(".forward").click(function(){
      step =  $('.current').attr('data-id');
      if(step == 1){
        
      }else if(step == 2){

        
        /*if ($("input:radio[name='ficci_member']").is(":checked")) {
          $("#ficci_option_error_message").hide();
          var sname = $("#form_ficci").val();
          if ($('input[name="ficci_member"]:checked').val() == 'Yes' && sname == '') {
            $( "#form_ficci" ).addClass( "required" );
            $("#form_ficci").attr("disabled", false);
            return false;   
          } else {
            $("#form_ficci").attr("disabled", true);
            $( "#form_ficci" ).removeClass( "required" );
            $("#ficci_error_message").hide();
          }
        }*/

        if ($("input:radio[name='gst']").is(":checked")) {
          $("#gst_option_error_message").hide();
          var sname = $("#form_gst").val();
          if ($('input[name="gst"]:checked').val() == 'Yes' && sname == '') {
            $( "#form_gst" ).addClass( "required" );
            $("#form_gst").attr("disabled", false);
            return false;   
          } else {
            $("#form_gst").attr("disabled", true);
            $( "#form_gst" ).removeClass( "required" );
            $("#gst_error_message").hide();
          }
        }else{
          /*$("#gst_option_error_message").html("*Do you have gst no.?*");
          $("#gst_option_error_message").show();*/
          return false;
        }
      }
      return false;
  });

      /*$('INPUT[type="file"]').change(function () {
          var ext = this.value.match(/\.(.+)$/)[1];
          switch (ext) {
              case 'jpg':
              case 'jpeg':
              case 'png':
              case 'gif':
                  $('#uploadButton').attr('disabled', false);
                  break;
              default:
                  $("#file_error_message").html("*This is not an allowed file type*");
                  $("#file_error_message").show();
                  var validate = false;           
                  this.value = '';
          }
      });*/
   </script>


  <script type="text/javascript">
    /*$(function () {
        $("input[name='ficci_member']").click(function () {
            if ($("#chkYes").is(":checked")) {
                $("#form_ficci").focus();

                $("#ficci_error_message").hide();

                var error_ficci=false;

                $("#form_ficci").focusout(function() {
                check_ficci();

                function check_ficci() {
            var pattern = /^[a-z0-9\\-]+$/;
            var sname = $("#form_ficci").val()
            if (pattern.test(sname) && sname !== '') {
               $("#ficci_error_message").hide();
               
            } else {
               $("#ficci_error_message").html("*Should contain only Characters and Numbers*");
               $("#ficci_error_message").show();
               
               error_ficci = true;
            }
         }

         });
            } 
            else{

                $("#ficci_error_message").hide();

                var error_ficci=false;

                $("#form_ficci").focusout(function() {
                check_ficci();

                function check_ficci() {
            var pattern = /[^0-9]/g;
            var sname = $("#form_mobile").val()
            if (pattern.test(sname) && sname !== '') {
               $("#ficci_error_message").hide();
               
            } else {
               $("#ficci_error_message").html("*Should contain only Characters and Numbers*");
               $("#ficci_error_message").hide();
               
               error_ficci = true;
            }
         }

         });
            }
        });
    });*/
</script>




<script>
$(document).ready(function(){

  


    $(".check-male").click(function(){
        $("#male").prop("checked", true);
    });
});
</script>

<!-- <script>
  

      $("#mobile_error_message").hide();

      var error_mobile = false;


      jQuery(document).ready(function($){
    $cf = $('#phonenumber');
    $cf.blur(function(e){
        phone = $(this).val();
        phone = phone.replace(/[^0-9]/,'');
        if (phone.length != 10)
        {
            $("#mobile_error_message").html("Invalid Mobile Number");
            $("#mobile_error_message").show();
            $('#phonenumber').val('');
            $('#phonenumber').focus();
        }
        else{

           $("#mobile_error_message").hide();
        }
    });
});
        

</script>


<script>
  

      $("#phone_error_message").hide();

      var error_phone = false;


      jQuery(document).ready(function($){
    $cf = $('#phone');
    $cf.blur(function(e){
        phone = $(this).val();
        phone = phone.replace(/[^0-9]/,'');
        if (phone.length != 10)
        {
            $("#phone_error_message").html("Invalid Mobile Number");
            $("#phone_error_message").show();
            $('#phone').val('');
            $('#phone').focus();
        }
        else{

           $("#phone_error_message").hide();
        }
    });
});
</script>



  <script type="text/javascript">
    $(function () {
        $("input[name='gst']").click(function () {
            if ($("#chkYes").is(":checked")) {
                $("#form_gst").focus();

                $("#gst_error_message").hide();

                var error_gst=false;

                $("#form_gst").focusout(function() {
                check_gst();

                function check_gst() {
            var pattern = /^[a-z0-9\\-]+$/;
            var sname = $("#form_gst").val()
            if (pattern.test(sname) && sname !== '') {
               $("#gst_error_message").hide();
               
            } else {
               $("#gst_error_message").html("*Should contain only Characters and Numbers*");
               $("#gst_error_message").show();
               
               error_gst = true;
            }
         }

         });
            } 
            else{

                $("#gst_error_message").hide();

                var error_gst=false;

                $("#form_gst").focusout(function() {
                check_gst();

                function check_gst() {
            var pattern = /[^0-9]/g;
            var sname = $("#form_mobile").val()
            if (pattern.test(sname) && sname !== '') {
               $("#gst_error_message").hide();
               
            } else {
               $("#gst_error_message").html("*Should contain only Characters and Numbers*");
               $("#gst_error_message").hide();
               
               error_gst = true;
            }
         }

         });
            }
        });
    });
</script> -->


    <script src="{{ asset('countdowntime/countdowntime/moment.min.js') }}"></script> 
    <script src="{{ asset('countdowntime/countdowntime/moment-timezone.min.js') }}"></script> 
    <script src="{{ asset('countdowntime/countdowntime/moment-timezone-with-data.min.js') }}"></script> 
    <script src="{{ asset('countdowntime/countdowntime/countdowntime.js') }}"></script> 
    <script>
        $('.cd100').countdown100({
          /*Set Endtime here*/
          /*Endtime must be > current time*/
          endtimeYear: 2020,
          endtimeMonth: 8,
          endtimeDate: 19,
          endtimeHours: 0,
          endtimeMinutes: 0,
          endtimeSeconds: 0,
          timeZone: "" 
          // ex:  timeZone: "America/New_York"
          //go to " http://momentjs.com/timezone/ " to get timezone
        });

        $(document).ready(function() {

        $('input[type=radio][name=ficci_member]').change(function() {
            if ($('input[name="ficci_member"]:checked').val()  == 'Yes') {
                $('#Amount').val(3000);
               $('#form_ficci').css("display", "");
               $("#form_ficci").attr("disabled", false);

               $("#ficci_radio_id"). prop("checked", true);
               $('#ficci_radio_id').attr('disabled', false);

               $('#nonficci_radio_id').attr('disabled', true);
               $('#academia_radio_id').attr('disabled', true);
            }
            else if ($('input[name="ficci_member"]:checked').val()  == 'No') {
                $('#Amount').val('');
                $('#form_ficci').css("display", "none");
                $("#form_ficci").attr("disabled", true);
                
                $("#ficci_radio_id"). prop("checked", false);
                $('#ficci_radio_id').attr('disabled', true);

                $('#nonficci_radio_id').attr('disabled', false);
                $('#academia_radio_id').attr('disabled', false);
                
                
            }
        });  

         $('input[type=radio][name=gst]').change(function() {
            if ($('input[name="gst"]:checked').val()  == 'Yes') {
              $("#form_gst").attr("disabled", false);
               $('#form_gst').css("display", "");
            }
            else if ($('input[name="gst"]:checked').val()  == 'No') {
              $("#form_gst").attr("disabled", true);
                $('#form_gst').css("display", "none");
            }
        }); 

         $('input[type=radio][name=payment_type]').change(function() {
            if ($('input[name="payment_type"]:checked').val()  == 'neft') {
               $('.paymentdetails_div').css("display", "");
            }
            else if ($('input[name="payment_type"]:checked').val()  == 'online') {
                $('.paymentdetails_div').css("display", "none");
            }
        }); 

          $('input[type=radio][name=payment_amount]').change(function() {
            if (this.value == 'ficci') {
                $('#Amount').val(3000);
            }
            else if (this.value == 'nonficci') {
                $('#Amount').val(3500);
            }
            else if (this.value == 'academia') {
                $('#Amount').val(750);
            }
        });

   $("body").on('click', '#paymentpro', function() {
        
        if ($("input:radio[name='payment_amount']").is(":checked")) {
          $("#payment_amount_error_message").hide();
           
        }else{
          $("#payment_amount_error_message").html("*Please enter your your Plan*");
          $("#payment_amount_error_message").show();
          return false;
        }

        if ($("input:radio[name='payment_type']").is(":checked")) {
          $("#payment_type_error_message").hide();
        }else{
          $("#payment_type_error_message").html("*Please enter payment option*");
          $("#payment_type_error_message").show();
          return false;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

       // e.preventDefault();
        var data = $( '.form_bg' ).serialize();

        var formData = new FormData($('.form_bg')[0]);
        formData.append('image', $('input[type=file]')[0].files[0]); 
        $('.register-error-msg').remove();
        $('.preloader').attr('style', '');
        $('.register-button-submit').attr('disabled', 'disabled');
        $.ajax({
            url: '/register-user',
            type: 'POST',
            data: formData,
            cache: false,
            contentType:false,
            processData: false,
            async:true,
            success: function(response) {
              var status = response.status;
              if(status == 'success') { 
                payment_typeval = $("input[name='payment_type']:checked"). val();
                $('#RegId').val(response.id);
                if(payment_typeval == 'online') {
                  $('.form_bg').submit(); 
                }else{
                  window.location = "/offlinepayment/"+response.id;
                }
                
              } else if(status == 'errors') {
                /*$('.alert-danger').show();
                $('.alert-danger strong').html(response.error);*/
              } else if(status == 'error') {
                $('.alert-danger').show();
                $('.alert-danger strong').html(response.error);
              }
            },
      error: function (jqXHR) {
        /*
        $('.register-loader').attr('style', 'display:none');
        $('.register-button-submit').removeAttr('disabled');
        var response = $.parseJSON(jqXHR.responseText);
        if(response.message) {
          if(response.errors) {
            var target = $('#registerfrom');
            $.each(response.errors, function(field, msg) {
              $('#'+field, $('#registerfrom', $('.form-side-padd'))).after('<span class="text-danger register-error-msg">'+msg+'</span>');
            });
          }
        }
        */
      }
        });
    return false;
    
});
   });
      </script> 

</body>
</html>
