@extends('layouts.exhibition')

@section('content')
<body class="page-template-carousel body_bg">
<div class="site-wrapper">
  <div class="doc-loader"></div>
  
  <!-- Left Part Sidebar -->
  <!--<div class="menu-left-part">
    <nav id="header-main-menu">
      <ul class="main-menu sm sm-clean">
        <li> <a href="index.html">Home</a> </li>
        <li> <a href="about.html">About</a> </li>
        <li> <a href="gallery.html" class="has-submenu">Gallery</a>
          <ul class="sub-menu">
            <li> <a href="gallery.html">Gallery Color</a> </li>
            <li> <a href="gallery-bw.html">Gallery B&amp;W</a> </li>
            <li> <a href="gallery-inverse-bw.html">Gallery W&amp;B</a> </li>
          </ul>
        </li>
        <li> <a href="blog.html">Blog</a> </li>
        <li> <a href="contact.html">Contact</a> </li>
      </ul>
      <form role="search" method="get" class="search-form" action="#">
        <label>
          <input autocomplete="off" type="search" class="search-field" placeholder="Search" value="" name="s" title="Search for:">
        </label>
      </form>
    </nav>
    <div class="menu-right-text">
      <p class="menu-text-title">HELLO</p>
      <div class="menu-text"> Welcome to our online art journey. You can read our <a href="blog.html">thoughts</a> or you can simply <a href="contact.html">write to us</a> </div>
      <br>
      <br>
      <a class="socail-text" href="#">TW.</a> <a class="socail-text" href="#">FB.</a> <a class="socail-text" href="#">IN.</a> <a class="socail-text" href="#">BE.</a> </div>
  </div>-->
  
  <!-- Right Part Sidebar -->
  <div class="menu-right-part">
    <div class="header-logo"> <a href="/"> <img src="images/logo.png" alt="Anotte"> </a> </div>
    <div class="backto-loby"><a href="/lobby">Back to Lobby</a></div>
    <!--<div class="toggle-holder">
      <div id="toggle">
        <div class="menu-line"></div>
      </div>
    </div>-->
  </div>
  <!--<a href="#" class="logo2" style="z-index:999;"></a>-->
  <div class="top_bar" style="z-index:999;">
<a href="#" class="top_bar_logo">FICCI HIC 2020</a>
<!--<div class="top_bar_search">
<div class="search">
      <input type="text" class="searchTerm" placeholder="Search">
      <button type="submit" class="searchButton">
        <i class="fa fa-search"></i>
     </button>
   </div>
</div>-->
<a href="#" class="top_bar_attending" data-toggle="modal" data-target="#myModal2">
<p><span>Now Attending</span> : 0.3k</p>
</a>
<a href="#" class="top_bar_attending" data-toggle="modal" data-target="#myModal2">
<p><span>This Location</span> : 1.2k</p>
</a>
<a href="#" class="top_bar_agenda" data-toggle="modal" data-target="#modal-agenda"><img src="{{ asset('img/calendar-icon.png') }}" width="24" /></a>
<!--<a href="#" class=""></a>-->
      <div id="dd" class="wrapper-dropdown-3 top_bar_agenda" tabindex="1"><img src="{{ asset('img/notifiction-icon.png') }}" width="17" />
        <ul class="dropdown">
          <li><a href="#">
          <div class="top-baar-notification">
          <div class="user_avatar"><img src="{{ asset('img/atul-sahai.png') }}"></div>
          <div class="comment_body">
          <p>Lorem Ipsum is simply dummy text of the printing. </p>
          </div>
          </div>
          </a></li>
          <li><a href="#">
          <div class="top-baar-notification">
          <div class="user_avatar"><img src="{{ asset('img/atul-sahai.png') }}"></div>
          <div class="comment_body">
          <p>Lorem Ipsum is simply dummy text of the printing. </p>
          </div>
          </div>
          </a></li>
          <li><a href="#">
          <div class="top-baar-notification">
          <div class="user_avatar"><img src="{{ asset('img/atul-sahai.png') }}"></div>
          <div class="comment_body">
          <p>Lorem Ipsum is simply dummy text of the printing. </p>
          </div>
          </div>
          </a></li>
        </ul>
      </div>
<!--<div class="dropdown">
<a href="#" class="top_bar_drop"><img src="img/topbar-arow.png" width="10" /></a>
<div class="dropdown-content">
    <a href="#"><img src="img/conversation-icon.png" width="18" /> Public group chat</a>
    <a href="#"><img src="img/trophy-icon.png" width="18" /> Leadership board</a>
  </div>
</div>-->
</div>
  <div class="right-iconbar" style="z-index:999;"> 
<a href="#" class="profile"><div class="profile_btn_green"></div></a>
<a href="#" class="right-iconbar-ml5"><img src="{{ asset('img/share.png') }}" width="24" /></a> 
<a href="#" class="right-iconbar-ml5"><img src="{{ asset('img/speak.png') }}" width="24" /></a> 
<a href="#" class="right-iconbar-ml5"><img src="{{ asset('img/fullscreen.png') }}" width="24" /></a> 
<a href="#" class="right-iconbar-nobot right-iconbar-ml5"><img src="{{ asset('img/setting.png') }}" width="24" /></a> 
</div>
  <div class="menu-bottom" style="z-index:999;"> <a href="/lobby" class="menu-icon"><img src="{{ asset('img/lobby.png') }}" width="36" />
  <p>Lobby</p>
  </a> <a href="#" class="menu-icon"><img src="{{ asset('img/falcon1hall.png') }}" width="36" />
  <p>Falcon 1 Hall</p>
  </a> <a href="/exhibition" class="menu-icon"><img src="{{ asset('img/exhibition.png') }}" width="36" />
  <p>Exhibition</p>
  </a> <!--<a href="#" class="menu-icon"><img src="{{ asset('img/irish-hall.png') }}" width="36" />
  <p>Irish Hall</p>
  </a>--> <a href="#" class="menu-icon"><img src="{{ asset('img/helpdesk.png') }}" width="36" />
  <p>Helpdesk</p>
  </a> <a href="#" class="menu-icon" data-toggle="modal" data-target="#modal-exhibition-tweetwall"><img src="{{ asset('img/social.png') }}" width="36" />
  <p>Social</p>
  </a> <a href="#" class="menu-icon" data-toggle="modal" data-target="#modal-exhibition-feedback"><img src="{{ asset('img/feedback.png') }}" width="36" />
  <p>Feedback</p>
  </a>
  <a href="#" class="menu-icon" data-toggle="modal" data-target="#modal-souvenir-booklet"><img src="{{ asset('img/souvenir-booklet.png') }}" width="36" />
  <p>Souvenir Booklet</p></a>
  <div class="dropup" style="float:right; width:6px;"> 
  <a href="#" class="menu-icon" style="margin-top:-5px;"><img src="{{ asset('img/dots.png') }}" width="6" style="margin-top:10px;" /></a> 
  <div class="dropup-content">
    <!--<a href="#"><img src="img/briefcase-icon.png" width="18" />  Briefcase</a>
    <a href="#"><img src="img/feedback-icon.png" width="18" />  Feedback</a>-->
    <a href="#"><img src="{{ asset('img/helpdesk-icon.png') }}" width="18" />  Helpdesk</a>
    <a href="#" data-toggle="modal" data-target="#myModal2"><img src="{{ asset('img/attendee-icon.png') }}" width="18" />  Attedee List</a>
  </div>
  </div>
  </div>
  
  <!-- Page Content Holder -->
  <div id="content" class="site-content">
    <div class="content-right">
      <div class="horizontal-slider image-slider-wrapper relative">
        <div class="swiper-wrapper image-slider slider">
          <div class="swiper-slide text-slide">
            <div class="carousel-item-text">
              <!--<p class="title-description-up">PHOTOGRAPHY</p>-->
              <h1 class="entry-title big-text">Exhibition Area</h1>
            </div>
          </div>
          <div class="swiper-slide">
            <div class="carousel-item-image"> <a href="#" class="img-thumb" data-toggle="modal" data-target="#manipalcigna-video2"> <img src="images/booth2.png" alt="" > </a>
              <div class="carousel-item-info exhibition-iconcenter">
                <div class="carousel-cat-links">
                  <ul>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#canipalcigna-i"><img src="images/info.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#canipalcigna-request-quote"><img src="images/request-quote-icon.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#manipalcigna-products"><img src="images/product-con.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#manipalcigna-video1"><img src="images/video-icon.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#modal-exhibition-briefcase"><img src="images/briefcase-icon.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#"><img src="images/business-card-icon.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#"><img src="images/chat-icon.png" width="45" height="45"></a> </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-slide">
            <div class="carousel-item-image"> <a href="#" class="img-thumb" data-toggle="modal" data-target="#manipalcigna-video2"> <img src="images/booth2.png" alt="" > </a>
              <div class="carousel-item-info exhibition-iconcenter">
                <div class="carousel-cat-links">
                  <ul>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#canipalcigna-i"><img src="images/info.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#canipalcigna-request-quote"><img src="images/request-quote-icon.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#manipalcigna-products"><img src="images/product-con.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#manipalcigna-video1"><img src="images/video-icon.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#modal-exhibition-briefcase"><img src="images/briefcase-icon.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#"><img src="images/business-card-icon.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#"><img src="images/chat-icon.png" width="45" height="45"></a> </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-slide">
            <div class="carousel-item-image"> <a href="#" class="img-thumb" data-toggle="modal" data-target="#manipalcigna-video2"> <img src="images/booth2.png" alt="" > </a>
              <div class="carousel-item-info exhibition-iconcenter">
                <div class="carousel-cat-links">
                  <ul>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#canipalcigna-i"><img src="images/info.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#canipalcigna-request-quote"><img src="images/request-quote-icon.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#manipalcigna-products"><img src="images/product-con.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#manipalcigna-video1"><img src="images/video-icon.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#" data-toggle="modal" data-target="#modal-exhibition-briefcase"><img src="images/briefcase-icon.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#"><img src="images/business-card-icon.png" width="45" height="45"></a> </li>
                    <li class="portfolio-category hvr-pulse"> <a href="#"><img src="images/chat-icon.png" width="45" height="45"></a> </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
         <!--<div class="swiper-slide">
            <div class="carousel-item-image"> <a href="portfolio-1.html" class="img-thumb"> <img src="images/image_04.jpg" alt="" > </a>
              <div class="carousel-item-info">
                <div class="carousel-cat-links">
                  <ul>
                    <li class="portfolio-category"> <a href="#">Inspiration</a> </li>
                    <li class="portfolio-category"> <a href="#">Portrait</a> </li>
                  </ul>
                </div>
                <h2> <a href="portfolio-1.html">Game Room</a> </h2>
              </div>
              <p class="post-num"> <span>04</span> <span class="total-num">05</span> </p>
            </div>
          </div>
          <div class="swiper-slide">
            <div class="carousel-item-image"> <a href="portfolio-2.html" class="img-thumb"> <img src="images/image_05.jpg" alt="" > </a>
              <div class="carousel-item-info">
                <div class="carousel-cat-links">
                  <ul>
                    <li class="portfolio-category"> <a href="#">Inspiration</a> </li>
                    <li class="portfolio-category"> <a href="#">Portrait</a> </li>
                  </ul>
                </div>
                <h2> <a href="portfolio-2.html">Work Station</a> </h2>
              </div>
              <p class="post-num"> <span>05</span> <span class="total-num">05</span> </p>
            </div>
          </div>-->
          <!--<div class="swiper-slide more-posts-portfolio">
            <div class="portfolio-slider-load-more"> <span class="more-portfolio-posts">LOAD MORE</span> </div>
          </div>-->
        </div>
      </div>
    </div>
    <div class="clear"></div>
    
  </div>
  <!-- End Page Content Holder -->
  <!--<div style="text-align:center; width:100% !important;  background:#ffffff !important; position:absolute; z-index:1;  border-top:solid 1px #e8e8e8;">
   <div class="center-relative content-90">
    <h6 style="font-size: 16px; padding:0 0 0px 0;">Exhibitor List</h6>
    <div class="logo-fit">
    <img src="images/dumy1.png">
    <img src="images/dumy2.png">
    <img src="images/dumy3.png">
    <img src="images/dumy4.png">
    <img src="images/dumy5.png">
    <img src="images/dumy6.png">
    <img src="images/dumy7.png">
    <img src="images/dumy2.png">
    <img src="images/dumy3.png">
    <img src="images/dumy4.png">
    <img src="images/dumy6.png">
    </div>
    <div class="clear"></div>
    </div>
    </div>-->
</div>

<!-- Popups -->
<!-- top bar pop--> 
<!-- Agenda pop-->
<div class="modal fade" id="modal-agenda" role="dialog">
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
        
        <div class="w3-bar">
  <button class="w3-bar-item w3-button agenda-tab-lineh w_color" onClick="openCity('agenda-monday-tab')"><span style="display:block; font-weight:600; font-size:18px;text-align: left;">19 AUGUST</span> Wednesday</button>
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
  <div id="agenda-monday-tab" class="w3-container city">
  <embed height="500" src="pdf/hic-agenda.pdf" style="width:100%;">
  </div>
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
<!-- profile pop-->
<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
        <div class="container scrollbar scrollbar-primary">
        <div class="row">
  <button type="button" class="close pro-close" data-dismiss="modal">&times;</button>
    <div class="col-lg-1 col-sm-12">Sort</div>
    <div class="col-lg-4 col-sm-6">
    <select class="pro-select">
    <option value="0">All</option>
    
  </select>
    </div>
    <div class="col-lg-4 col-sm-6">
    <div class="search">
      <input type="text" class="searchTerm" placeholder="Search">
      <button type="submit" class="searchButton">
        <i class="fa fa-search"></i>
     </button>
   </div>
    </div>
    
    <div class="pro-bor-bot"></div>
    <div class="clear"></div>
    <div class="col-lg-4 col-sm-6">
      <div class="card hovercard">
        <div class="cardheader"> </div>
        <div class="info">
        <div class="avatar"> <img alt="" src="http://lorempixel.com/100/100/people/9/"> </div>
          <div class="title"> <a target="_blank" href="https://scripteden.com/">Script Eden</a> </div>
          <div class="desc">Passionate designer</div>
          <div class="desc">Curious developer</div>
          <div class="desc">Tech geek</div>
        <div class="bottom"> 
        <a class="" href="#" style="margin-right:5px;"><img src="../img/in.png" width="18" /></a> 
        <a class="" href="#" style="margin-right:5px;"><img src="../img/tw.png" width="20" /></a> 
        <a class="" href="#"><img src="../img/chat-icon.png" width="22" /></a> 
        </div>
        </div>
        
      </div>
    </div>
    <div class="col-lg-4 col-sm-6">
      <div class="card hovercard">
        <div class="cardheader"> </div>
        <div class="info">
        <div class="avatar"> <img alt="" src="http://lorempixel.com/100/100/people/9/"> </div>
          <div class="title"> <a target="_blank" href="https://scripteden.com/">Script Eden</a> </div>
          <div class="desc">Passionate designer</div>
          <div class="desc">Curious developer</div>
          <div class="desc">Tech geek</div>
        <div class="bottom"> 
        <a class="" href="#" style="margin-right:5px;"><img src="../img/in.png" width="18" /></a> 
        <a class="" href="#" style="margin-right:5px;"><img src="../img/tw.png" width="20" /></a> 
        <a class="" href="#"><img src="../img/chat-icon.png" width="22" /></a> 
        </div>
        </div>
        
      </div>
    </div>
    <div class="col-lg-4 col-sm-6">
      <div class="card hovercard">
        <div class="cardheader"> </div>
        <div class="info">
        <div class="avatar"> <img alt="" src="http://lorempixel.com/100/100/people/9/"> </div>
          <div class="title"> <a target="_blank" href="https://scripteden.com/">Script Eden</a> </div>
          <div class="desc">Passionate designer</div>
          <div class="desc">Curious developer</div>
          <div class="desc">Tech geek</div>
        <div class="bottom"> 
        <a class="" href="#" style="margin-right:5px;"><img src="../img/in.png" width="18" /></a> 
        <a class="" href="#" style="margin-right:5px;"><img src="../img/tw.png" width="20" /></a> 
        <a class="" href="#"><img src="../img/chat-icon.png" width="22" /></a> 
        </div>
        </div>
        
      </div>
    </div>
    
    <div class="col-lg-4 col-sm-6">
      <div class="card hovercard">
        <div class="cardheader"> </div>
        <div class="info">
        <div class="avatar"> <img alt="" src="http://lorempixel.com/100/100/people/9/"> </div>
          <div class="title"> <a target="_blank" href="https://scripteden.com/">Script Eden</a> </div>
          <div class="desc">Passionate designer</div>
          <div class="desc">Curious developer</div>
          <div class="desc">Tech geek</div>
        <div class="bottom"> 
        <a class="" href="#" style="margin-right:5px;"><img src="../img/in.png" width="18" /></a> 
        <a class="" href="#" style="margin-right:5px;"><img src="../img/tw.png" width="20" /></a> 
        <a class="" href="#"><img src="../img/chat-icon.png" width="22" /></a> 
        </div>
        </div>
        
      </div>
    </div>
    <div class="col-lg-4 col-sm-6">
      <div class="card hovercard">
        <div class="cardheader"> </div>
        <div class="info">
        <div class="avatar"> <img alt="" src="http://lorempixel.com/100/100/people/9/"> </div>
          <div class="title"> <a target="_blank" href="https://scripteden.com/">Script Eden</a> </div>
          <div class="desc">Passionate designer</div>
          <div class="desc">Curious developer</div>
          <div class="desc">Tech geek</div>
        <div class="bottom"> 
        <a class="" href="#" style="margin-right:5px;"><img src="../img/in.png" width="18" /></a> 
        <a class="" href="#" style="margin-right:5px;"><img src="../img/tw.png" width="20" /></a> 
        <a class="" href="#"><img src="../img/chat-icon.png" width="22" /></a> 
        </div>
        </div>
        
      </div>
    </div>
    <div class="col-lg-4 col-sm-6">
      <div class="card hovercard">
        <div class="cardheader"> </div>
        <div class="info">
        <div class="avatar"> <img alt="" src="http://lorempixel.com/100/100/people/9/"> </div>
          <div class="title"> <a target="_blank" href="https://scripteden.com/">Script Eden</a> </div>
          <div class="desc">Passionate designer</div>
          <div class="desc">Curious developer</div>
          <div class="desc">Tech geek</div>
        <div class="bottom"> 
        <a class="" href="#" style="margin-right:5px;"><img src="../img/in.png" width="18" /></a> 
        <a class="" href="#" style="margin-right:5px;"><img src="../img/tw.png" width="20" /></a> 
        <a class="" href="#"><img src="../img/chat-icon.png" width="22" /></a> 
        </div>
        </div>
        
      </div>
    </div>
    
    <div class="col-lg-4 col-sm-6">
      <div class="card hovercard">
        <div class="cardheader"> </div>
        <div class="info">
        <div class="avatar"> <img alt="" src="http://lorempixel.com/100/100/people/9/"> </div>
          <div class="title"> <a target="_blank" href="https://scripteden.com/">Script Eden</a> </div>
          <div class="desc">Passionate designer</div>
          <div class="desc">Curious developer</div>
          <div class="desc">Tech geek</div>
        <div class="bottom"> 
        <a class="" href="#" style="margin-right:5px;"><img src="../img/in.png" width="18" /></a> 
        <a class="" href="#" style="margin-right:5px;"><img src="../img/tw.png" width="20" /></a> 
        <a class="" href="#"><img src="../img/chat-icon.png" width="22" /></a> 
        </div>
        </div>
        
      </div>
    </div>
    
  </div>
        </div>
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
<!-- top bar end pop-->

<!-- ManipalCigna Profile i pop-->  
<div class="modal fade" id="canipalcigna-i" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close pro-close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mb-0 mt-0 pt-0 pb-0 center exhibition_video_pop_head">Exhibitor Profile</h4>
      </div>
        <div class="modal-body">
        <div class="container">
        <div class="row">
        <div class="col-lg-4 col-sm-6 exhibitor-profile-i"><img class="img-left" src="images/manipalcigna/manipal-cigna.png"/></div>
        <div class="col-lg-6 col-sm-6">
          <p class="text-white mb-1">ManipalCigna Health Insurance Company Limited Corporate Office</p>
          <p class="text-white mb-1">Location : Mumbai</p>
          <p class="text-white mb-1">Sector : Health Insurance</p>
          <p class="text-white mb-1">City : Mumbai</p>
          <p class="text-white mb-0">Website :  www.manipalcigna.com</p>
        </div>
        </div>
        <div class="row">
        <div class="exhibitor-profile-i-bor"></div>
        <div class="col-lg-8 col-sm-6">
        <h6 class="text-white p-0">About ManipalCigna Health Insurance Company Limited</h6>
        <p class="text-white mb-2">ManipalCigna Health insurance Company Limited (formerly known as CignaTTK Health Insurance
Company Limited) is a joint venture between the Manipal Group, an eminent player in the field of
healthcare delivery and higher education in India and Cigna Corporation, a global health services
company with presence in 30+ countries and serving 180 million+ customers around the world. With
a deep focus on health and wellness, ManipalCigna Health Insurance offers a full suite of insurance
solutions ranging from health, personal accident, major illness, travel and global care to individual
customers, employer-employee, and non-employer-employee groups to meet their diverse health
needs. ManipalCigna understands its customers' needs and in line with its mission to improve the
health, well-being and peace of mind of those it serves, the company provides a variety of wellness
programs to guide its customers along the road to physical, emotional and financial well-being.</p>
        
        </div>
        <div class="col-lg-4 col-sm-6">
        <h6 class="text-white p-0">Authorized person</h6>
          <p class="text-white mb-0">ManipalCigna Health Insurance Company Limited Corporate Office</p>
          <p class="text-white mb-0">Toll free : 1800-102-4462</p>
          <p class="text-white mb-0">Email : customercare@manipalcigna.com</p>
          <p class="text-white mb-0">Website : www.manipalcigna.com</p>
        <h6 class="text-white p-0 mt-3">Social Media</h6>
        <div>
        <a href="https://www.facebook.com/ManipalCignaHealthInsurance/" target="_blank" class="fa fa-facebook"></a>
        <a href="https://twitter.com/ManipalCigna?s=03" target="_blank" class="fa fa-twitter"></a>
        <a href="https://instagram.com/manipalcigna_healthinsurance" target="_blank" class="fa fa-instagram"></a>
        <a href="https://www.linkedin.com/company/manipalcigna-health-insurance" target="_blank" class="fa fa-linkedin"></a>
        <a href="https://www.youtube.com/user/CignaTTKHealth" target="_blank" class="fa fa-youtube"></a>
        </div>
        </div>
        <div class="clear"></div>
        
        </div>
        </div>
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div> 
<!-- request quote pop-->  
<div class="modal fade" id="canipalcigna-request-quote" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close pro-close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mb-0 mt-0 pt-0 pb-0 center exhibition_video_pop_head">Request a quote</h4>
      </div>
        <div class="modal-body">
        <div class="container">
        <form>
        <div class="row">
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Name</label>
        <div class="col-lg-6 col-sm-6"><p class="request_a_quote_pop_head">R K Malhotra</p></div>
        <div class="clear"></div>
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Contact Us</label>
        <div class="col-lg-6 col-sm-6"><p class="request_a_quote_pop_head">rajesh.malhotra@rishirajmedia.com</p></div>
        <div class="clear"></div>
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Mobile No.*</label>
        <div class="col-lg-6 col-sm-6"><p class="request_a_quote_pop_head">+91 9810077205</p></div>
        <div class="clear"></div>
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Subject</label>
        <div class="col-lg-8 col-sm-6 mb-3"><input type="text" name="subject" value="" required="" class="form-control"></div>
        <div class="clear"></div>
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Message</label>
        <div class="col-lg-8 col-sm-6 mb-3"><textarea name="message" class="form-control" required="" style="height:100px;"></textarea></div>
        <div class="clear"></div>
        <div class="col-lg-3 col-sm-6"></div>
        <div class="col-lg-8 col-sm-6" style="text-align:right;"><input type="submit" value="Send" class="btn btn-danger"></div>
        </div>
        </form>
        </div>
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
<!-- Products pop -->
<div class="modal fade" id="manipalcigna-products" role="dialog">
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mb-0 mt-0 pt-0 pb-0 exhibition_video_pop_head">Products</h4>
      </div>
        <div class="modal-body">
        <div class="owl-carousel owl-theme">
<div class="item exhibition-products">
<img src="images/manipalcigna/ProHealth_Accordian_No-19-1.jpg" />
<p class="mb-3">Health insurance in India is the fastest growing segment and projected.</p>
<p><a href="https://www.manipalcigna.com/" target="_blank" class="exhibition-products-btn">Visit Website</a></p>
</div>

<div class="item exhibition-products">
<img src="images/manipalcigna/SuperTopUp_Accordian_Dec19_Web-1.jpg" />
<p class="mb-3">Health insurance in India is the fastest growing segment and projected.</p>
<p><a href="https://www.manipalcigna.com/" target="_blank" class="exhibition-products-btn">Visit Website</a></p>
</div>
        
</div>
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
<!-- manipalcigna-video1 pop-->  
<div class="modal fade" id="manipalcigna-video1" role="dialog">
    <div class="modal-dialog  modal-xlg">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close pro-close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mb-0 mt-0 pt-0 pb-0 center exhibition_video_pop_head">ManipalCigna</h4>
      </div>
        <div class="modal-body">
        <div class="container">
        <div class="row">
        <div class="col-lg-6 col-sm-12">
        <div class="exhibition_video_pop">
        <iframe height="360" src="https://www.youtube.com/embed/GOnoBBapNsE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
        </div>
        </div>
        <div class="col-lg-6 col-sm-12">
        <div class="exhibition_video_pop">
        <iframe height="360" src="https://www.youtube.com/embed/Wh2K5ur3A7w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
        </div>
        </div>
        </div>
        </div>
        <div class="exhibition_video_pop"><a href="#">Skip the video</a></div>
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
<!-- manipalcigna-video2 pop-->  
<div class="modal fade" id="manipalcigna-video2" role="dialog">
    <div class="modal-dialog  modal-md">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close pro-close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mb-0 mt-0 pt-0 pb-0 center exhibition_video_pop_head">ManipalCigna</h4>
      </div>
        <div class="modal-body">
        <div class="container">
        <div class="row">
        <div class="col-lg-12 col-sm-12">
        <div class="exhibition_video_pop">
        <iframe height="360" src="https://www.youtube.com/embed/PohDIe8kZeE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
        
        </div>
        </div>
        </div>
        </div>
        <div class="exhibition_video_pop"><a href="#">Skip the video</a></div>
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
<!-- briefcase pop -->
<div class="modal fade" id="modal-exhibition-briefcase" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mb-0 mt-0 pt-0 pb-0 exhibition_video_pop_head">Briefcase</h4>
      </div>
        <div class="modal-body">
        <div class="briefcase_download">
        <div class="down_box1"><img src="images/pdficon.png" width="30"></div>
        <div class="down_box2">ProHealth Accordian Nov19</div>
        <div class="down_box4"><a href="images/manipalcigna/ProHealth_Accordian_Nov19.pdf" target="_blank"><img src="images/downloadicon.png"></a></div>
        <div class="down_box3">3.6mb</div>
        <div class="clear"></div>
        </div>
        
        <div class="briefcase_download">
        <div class="down_box1"><img src="images/pdficon.png" width="30"></div>
        <div class="down_box2">SuperTopUp Accordian Dec19</div>
        <div class="down_box4"><a href="images/manipalcigna/SuperTopUp_Accordian_Dec19_Web.pdf" target="_blank"><img src="images/downloadicon.png"></a></div>
        <div class="down_box3">3.6mb</div>
        <div class="clear"></div>
        </div>
        
        <!--<div class="briefcase_download">
        <div class="down_box1"><img src="images/driveicon.png" width="30"></div>
        <div class="down_box2">file_name.pdf</div>
        <div class="down_box4"><a href="#"><img src="images/downloadicon.png"></a></div>
        <div class="down_box3">3.6mb</div>
        <div class="clear"></div>
        </div>
        
        <div class="briefcase_download">
        <div class="down_box1"><img src="images/image-icon.png" width="30"></div>
        <div class="down_box2">file_name.pdf</div>
        <div class="down_box4"><a href="#"><img src="images/downloadicon.png"></a></div>
        <div class="down_box3">3.6mb</div>
        <div class="clear"></div>
        </div>
        
        <div class="briefcase_download">
        <div class="down_box1"><img src="images/pdficon.png" width="30"></div>
        <div class="down_box2">file_name.pdf</div>
        <div class="down_box4"><a href="#"><img src="images/downloadicon.png"></a></div>
        <div class="down_box3">3.6mb</div>
        <div class="clear"></div>
        </div>
        
        <div class="briefcase_download">
        <div class="down_box1"><img src="images/gmailicon.png" width="30"></div>
        <div class="down_box2">file_name.pdf</div>
        <div class="down_box4"><a href="#"><img src="images/downloadicon.png"></a></div>
        <div class="down_box3">3.6mb</div>
        <div class="clear"></div>
        </div>
        
        <div class="briefcase_download">
        <div class="down_box1"><img src="images/driveicon.png" width="30"></div>
        <div class="down_box2">file_name.pdf</div>
        <div class="down_box4"><a href="#"><img src="images/downloadicon.png"></a></div>
        <div class="down_box3">3.6mb</div>
        <div class="clear"></div>
        </div>-->
        
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
  
<!-- bottom bar pop-->
<!-- feedback pop-->  
<div class="modal fade" id="modal-exhibition-feedback" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close pro-close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mb-0 mt-0 pt-0 pb-0 center exhibition_video_pop_head">Feedback</h4>
      </div>
        <div class="modal-body">
        <div class="container">
        <form>
        <div class="row">
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Name</label>
        <div class="col-lg-6 col-sm-6"><p class="request_a_quote_pop_head">R K Malhotra</p></div>
        <div class="clear"></div>
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Contact Us</label>
        <div class="col-lg-6 col-sm-6"><p class="request_a_quote_pop_head">rajesh.malhotra@rishirajmedia.com</p></div>
        <div class="clear"></div>
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Mobile No.*</label>
        <div class="col-lg-6 col-sm-6"><p class="request_a_quote_pop_head">+91 9810077205</p></div>
        <div class="clear"></div>
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Subject</label>
        <div class="col-lg-8 col-sm-6 mb-3"><input type="text" name="subject" value="" required="" class="form-control"></div>
        <div class="clear"></div>
        <label class="control-label col-lg-3 col-sm-6 request_a_quote_pop_head">Message</label>
        <div class="col-lg-8 col-sm-6 mb-3"><textarea name="message" class="form-control" required="" style="height:100px;"></textarea></div>
        <div class="clear"></div>
        <div class="col-lg-3 col-sm-6"></div>
        <div class="col-lg-8 col-sm-6" style="text-align:right;"><input type="submit" value="Send" class="btn btn-danger"></div>
        </div>
        </form>
        </div>
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
<!-- tweet wall pop-->  
<div class="modal fade" id="modal-exhibition-tweetwall" role="dialog">
    <div class="modal-dialog modal-xlg">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close pro-close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mb-0 mt-0 pt-0 pb-0 center exhibition_video_pop_head">Tweet wall</h4>
      </div>
        <div class="modal-body">
        <div class="tweet_wall">
          <img src="{{ asset('img/tweet-wall.jpg') }}">
          </div>
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div>
<!-- Souvenir Booklet -->
<div class="modal fade" id="modal-souvenir-booklet" role="dialog">
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close pro-close" data-dismiss="modal">&times;</button>
      </div>
        <div class="modal-body">
        <embed height="500" src="pdf/HIC-Souvenir-Booklet.pdf" style="width:100%;">
        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>
      
    </div>
  </div> 
<!-- bottom bar end pop-->
<!-- Popups End-->

@endsection