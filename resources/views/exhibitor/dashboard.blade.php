@extends('layouts.app')

@section('content')
<!-- Begin page -->
<div id="wrapper"> 
    
    <!-- Top Bar Start -->
    <div class="topbar"> 
    
    <!-- LOGO -->
    <div class="topbar-left"> <a href="index.html" class="logo"> <img class="img-responsive" src="../assets/images/optimize-logo.png"> </a> </div>
    <nav class="navbar-custom">
        <ul class="navbar-right list-inline float-right mb-0">
        
        <!-- notification -->
        
        <li class="dropdown notification-list list-inline-item">
            <div class="dropdown notification-list nav-pro-img"> <a class="dropdown-toggle nav-link arrow-none nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"> <img src="../assets/images/companyicon.png" alt="user" class="rounded-circle"> </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown "> 
                <!-- item--> 
                
                <a class="dropdown-item text-danger" href="#"><i class="mdi mdi-power text-danger"></i> Logout</a> </div>
          </div>
          </li>
      </ul>
        <ul class="list-inline menu-left mb-0">
        <li class="float-left mobile-view">
            <button class="button-menu-mobile open-left waves-effect"> <i class="mdi mdi-menu"></i> </button>
          </li>
        <!--  <li class="d-none d-md-inline-block">
                        <form role="search" class="app-search">
                            <div class="form-group mb-0">
                                <input type="text" class="form-control" placeholder="Search..">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </li> -->
      </ul>
      </nav>
  </div>
    <!-- Top Bar End --> 
    
    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll"> 
        
        <!--- Sidemenu -->
        <div id="sidebar-menu"> 
        <!-- Left Menu Start -->
        <ul class="metismenu" id="side-menu">
            <li> <a href="index.html" class="waves-effect"> <i class="icon-squares"></i> <span> Dashboard </span> </a> </li>
            <li> <a href="quotation-requests.html" class="waves-effect"><i class="icon-calendar"></i><span>Quotation Requests</span></a> </li>
            <li> <a href="business-card.html" class="waves-effect"><i class="icon-calendar"></i><span>Business Card</span></a> </li>
            <li> <a href="customer-chat.html" class="waves-effect"><i class="icon-calendar"></i><span>Customer Chat</span></a> </li>
            
          </ul>
      </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>
      </div>
    <!-- Sidebar -left --> 
    
  </div>
    <!-- Left Sidebar End --> 
    
    <!-- ============================================================== --> 
    <!-- Start right Content here --> 
    <!-- ============================================================== -->
    <div class="content-page"> 
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid"> 
        
        <!-- end page-title -->
        <div class="row">
            <div class="col-sm-6 col-xl-12">
            <div class="card">
                <div class="card-heading p-4 text-center">
                <div class="row">
                <div class="col-sm-6 mb-3" style="float:right"><h5 style="text-align:left;color:#682FF0; font-weight:600">Expo</h5></div>
                <div class="col-sm-6 mb-3" style="float:right; text-align:right;"> <span>
                  <button type="button" class="btn btn-outline-primary waves-effect waves-light">Export in Excel</button>
                  </span> <span>
                  <button type="button" class="btn btn-outline-primary waves-effect waves-light">Export in PDF</button>
                  </span></div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xl-2 text-center">
                    <div>
                        <h5 class="font-16">No. of Exhibitors</h5>
                      </div>
                    <div class="mini-stat-icon text-center circle-element" > <span>5 companies</span> </div>
                  </div>
                    <div class="col-sm-6 col-xl-2 text-center">
                    <div>
                        <h5 class="font-16">Total No. Of Booth Visits</h5>
                      </div>
                    <div class="mini-stat-icon text-center circle-element" > <span >250 People</span> </div>
                  </div>
                    <div class="col-sm-6 col-xl-2 text-center">
                    <div>
                        <h5 class="font-16">Total Visiting Cards</h5>
                      </div>
                    <div class="mini-stat-icon text-center circle-element" > <span >250 People</span> </div>
                  </div>
                    <div class="col-sm-6 col-xl-2 text-center">
                    <div>
                        <h5 class="font-16">Total Documents Downloaded</h5>
                      </div>
                    <div class="mini-stat-icon text-center circle-element"> <span>250 Documents</span> </div>
                  </div>
                    <div class="col-sm-6 col-xl-2 text-center">
                    <div>
                        <h5 class="font-16">Total Video Views</h5>
                      </div>
                    <div class="mini-stat-icon text-center circle-element" > <span>250 Views</span> </div>
                  </div>
                  </div>
                <div class="row" style="margin-top:40px;">
                    <div class="col-sm-6 col-xl-2 text-center">
                    <div>
                        <h5 class="font-16">Total Products Viewed</h5>
                      </div>
                    <div class="mini-stat-icon text-center circle-element"> <span>250 Products</span> </div>
                  </div>
                    <div class="col-sm-6 col-xl-2 text-center">
                    <div>
                        <h5 class="font-16">No. of website Clicks</h5>
                      </div>
                    <div class="mini-stat-icon text-center circle-element"> <span>250 Clicks</span> </div>
                  </div>
                    
                  </div>
              </div>
              </div>
          </div>
            
            <!-- end col --> 
            
          </div>
        <!-- end row -->
        
        
        <!-- end row --> 
        
      </div>
        <!-- container-fluid --> 
        
      </div>
    <!-- content --> 
    
    <!-- content -->
    <footer class="footer"> © 2020 <span class="d-none d-sm-inline-block"> by Optimize</span>. </footer>
  </div>
@endsection
