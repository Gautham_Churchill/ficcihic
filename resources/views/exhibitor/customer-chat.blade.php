@extends('layouts.app')

@section('content')
<!-- Begin page -->
<div id="wrapper"> 
  
  <!-- Top Bar Start -->
  <div class="topbar"> 
    
    <!-- LOGO -->
    <div class="topbar-left"> <a href="index.html" class="logo"> <img class="img-thumbnail" src="../assets/images/optimize-logo.png"> </a> </div>
    <nav class="navbar-custom">
      <ul class="navbar-right list-inline float-right mb-0">
        <li class="dropdown notification-list list-inline-item">
          <div class="dropdown notification-list nav-pro-img"> <a class="dropdown-toggle nav-link arrow-none nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"> <img src="../assets/images/companyicon.png" alt="user" class="rounded-circle"> </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown "> <a class="dropdown-item text-danger" href="#"><i class="mdi mdi-power text-danger"></i> Logout</a> </div>
          </div>
        </li>
      </ul>
      <ul class="list-inline menu-left mb-0">
        <li class="float-left mobile-view">
          <button class="button-menu-mobile open-left waves-effect"> <i class="mdi mdi-menu"></i> </button>
        </li>
        <!--  <li class="d-none d-md-inline-block">
                        <form role="search" class="app-search">
                            <div class="form-group mb-0">
                                <input type="text" class="form-control" placeholder="Search..">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </li> -->
      </ul>
    </nav>
  </div>
  <!-- Top Bar End --> 
  
  <!-- ========== Left Sidebar Start ========== -->
  <div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll"> 
      
      <!--- Sidemenu -->
      <div id="sidebar-menu"> 
        <!-- Left Menu Start -->
        <ul class="metismenu" id="side-menu">
            <li> <a href="index.html" class="waves-effect"> <i class="icon-squares"></i> <span> Dashboard </span> </a> </li>
            <li> <a href="quotation-requests.html" class="waves-effect"><i class="icon-calendar"></i><span>Quotation Requests</span></a> </li>
            <li> <a href="business-card.html" class="waves-effect"><i class="icon-calendar"></i><span>Business Card</span></a> </li>
            <li> <a href="customer-chat.html" class="waves-effect"><i class="icon-calendar"></i><span>Customer Chat</span></a> </li>
        </ul>
      </div>
      <!-- Sidebar -->
      <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left --> 
    
  </div>
  <!-- Left Sidebar End --> 
  
  <!-- ============================================================== --> 
  <!-- Start right Content here --> 
  <!-- ============================================================== -->
  <div class="content-page"> 
    <!-- Start content -->
    <div class="content">
      <div class="container-fluid"> 
        <!-- end page-title -->
        
        <div class="row">
          <div id="frame">
            <div id="sidepanel">
              <div id="profile" class="">
                <div class="wrap"> <img id="profile-img" src="http://emilcarlsson.se/assets/mikeross.png" class="online" alt="">
                  <p>Mike Ross</p>
                  <!-- <i class="fa fa-chevron-down expand-button" aria-hidden="true"></i> -->
                  <div id="status-options">
                    <ul>
                      <li id="status-online" class="active"><span class="status-circle"></span>
                        <p>Online</p>
                      </li>
                      <li id="status-away"><span class="status-circle"></span>
                        <p>Away</p>
                      </li>
                      <li id="status-busy"><span class="status-circle"></span>
                        <p>Busy</p>
                      </li>
                      <li id="status-offline"><span class="status-circle"></span>
                        <p>Offline</p>
                      </li>
                    </ul>
                  </div>
                  <div id="expanded">
                    <label for="twitter"><i class="fa fa-facebook fa-fw" aria-hidden="true"></i></label>
                    <input name="twitter" type="text" value="mikeross">
                    <label for="twitter"><i class="fa fa-twitter fa-fw" aria-hidden="true"></i></label>
                    <input name="twitter" type="text" value="ross81">
                    <label for="twitter"><i class="fa fa-instagram fa-fw" aria-hidden="true"></i></label>
                    <input name="twitter" type="text" value="mike.ross">
                  </div>
                </div>
              </div>
              <div id="search">
                <label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
                <input type="text" placeholder="Search contacts...">
              </div>
              <div id="contacts" class="">
                <ul style="list-style:none">
                  <li class="contact">
                    <div class="wrap"> <span class="contact-status online"></span> <img src="http://emilcarlsson.se/assets/louislitt.png" alt="">
                      <div class="meta">
                        <p class="name">Louis Litt</p>
                        <p class="preview">You just got LITT up, Mike.</p>
                      </div>
                    </div>
                  </li>
                  <li class="contact active">
                    <div class="wrap"> <span class="contact-status busy"></span> <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="">
                      <div class="meta">
                        <p class="name">Harvey Specter</p>
                        <p class="preview">Wrong. You take the gun, or you pull out a bigger one. Or, you call their bluff. Or, you do any one of a hundred and forty six other things.</p>
                      </div>
                    </div>
                  </li>
                  <li class="contact">
                    <div class="wrap"> <span class="contact-status away"></span> <img src="http://emilcarlsson.se/assets/rachelzane.png" alt="">
                      <div class="meta">
                        <p class="name">Rachel Zane</p>
                        <p class="preview">I was thinking that we could have chicken tonight, sounds good?</p>
                      </div>
                    </div>
                  </li>
                  <li class="contact">
                    <div class="wrap"> <span class="contact-status online"></span> <img src="http://emilcarlsson.se/assets/donnapaulsen.png" alt="">
                      <div class="meta">
                        <p class="name">Donna Paulsen</p>
                        <p class="preview">Mike, I know everything! I'm Donna..</p>
                      </div>
                    </div>
                  </li>
                  <li class="contact">
                    <div class="wrap"> <span class="contact-status busy"></span> <img src="http://emilcarlsson.se/assets/jessicapearson.png" alt="">
                      <div class="meta">
                        <p class="name">Jessica Pearson</p>
                        <p class="preview">Have you finished the draft on the Hinsenburg deal?</p>
                      </div>
                    </div>
                  </li>
                  <li class="contact">
                    <div class="wrap"> <span class="contact-status"></span> <img src="http://emilcarlsson.se/assets/haroldgunderson.png" alt="">
                      <div class="meta">
                        <p class="name">Harold Gunderson</p>
                        <p class="preview">Thanks Mike! :)</p>
                      </div>
                    </div>
                  </li>
                  <li class="contact">
                    <div class="wrap"> <span class="contact-status"></span> <img src="http://emilcarlsson.se/assets/danielhardman.png" alt="">
                      <div class="meta">
                        <p class="name">Daniel Hardman</p>
                        <p class="preview">We'll meet again, Mike. Tell Jessica I said 'Hi'.</p>
                      </div>
                    </div>
                  </li>
                  <li class="contact">
                    <div class="wrap"> <span class="contact-status busy"></span> <img src="http://emilcarlsson.se/assets/katrinabennett.png" alt="">
                      <div class="meta">
                        <p class="name">Katrina Bennett</p>
                        <p class="preview">I've sent you the files for the Garrett trial.</p>
                      </div>
                    </div>
                  </li>
                  <li class="contact">
                    <div class="wrap"> <span class="contact-status"></span> <img src="http://emilcarlsson.se/assets/charlesforstman.png" alt="">
                      <div class="meta">
                        <p class="name">Charles Forstman</p>
                        <p class="preview">Mike, this isn't over.</p>
                      </div>
                    </div>
                  </li>
                  <li class="contact">
                    <div class="wrap"> <span class="contact-status"></span> <img src="http://emilcarlsson.se/assets/jonathansidwell.png" alt="">
                      <div class="meta">
                        <p class="name">Jonathan Sidwell</p>
                        <p class="preview"><span>You:</span> That's bullshit. This deal is solid.</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="content">
              <div class="contact-profile"> <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="">
                <p class="mt-2">Harvey Specter</p>
                <!-- <div class="social-media">
        <i class="fa fa-facebook" aria-hidden="true"></i>
        <i class="fa fa-twitter" aria-hidden="true"></i>
         <i class="fa fa-instagram" aria-hidden="true"></i>
      </div> --> 
              </div>
              <div class="messages">
                <ul>
                  <li class="sent"> <img src="http://emilcarlsson.se/assets/mikeross.png" alt="">
                    <p>How the hell am I supposed to get a jury to believe you when I am not even sure that I do?!</p>
                  </li>
                  <li class="replies"> <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="">
                    <p>When you're backed against the wall, break the god damn thing down.</p>
                  </li>
                  <li class="replies"> <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="">
                    <p>Excuses don't win championships.</p>
                  </li>
                  <li class="sent"> <img src="http://emilcarlsson.se/assets/mikeross.png" alt="">
                    <p>Oh yeah, did Michael Jordan tell you that?</p>
                  </li>
                  <li class="replies"> <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="">
                    <p>No, I told him that.</p>
                  </li>
                  <li class="replies"> <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="">
                    <p>What are your choices when someone puts a gun to your head?</p>
                  </li>
                  <li class="sent"> <img src="http://emilcarlsson.se/assets/mikeross.png" alt="">
                    <p>What are you talking about? You do what they say or they shoot you.</p>
                  </li>
                  <li class="replies"> <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="">
                    <p>Wrong. You take the gun, or you pull out a bigger one. Or, you call their bluff. Or, you do any one of a hundred and forty six other things.</p>
                  </li>
                </ul>
              </div>
              <div class="message-input">
                <div class="wrap">
                  <input type="text" placeholder="Write your message...">
                  <i class="fa fa-paperclip attachment" aria-hidden="true"></i>
                  <button class="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <!-- end row --> 
        
        <!-- START ROW --> 
        
        <!-- END ROW --> 
        
      </div>
      <!-- container-fluid --> 
      
    </div>
    <!-- content -->
    <footer class="footer"> © 2020 <span class="d-none d-sm-inline-block"> by Optimize</span>. </footer>
  </div>
  <!-- ============================================================== --> 
  <!-- End Right content here --> 
  <!-- ============================================================== --> 
  
</div>
<!-- END wrapper --> 
@endsection
