@extends('layouts.app')

@section('content')

<!-- Begin page -->
<div id="wrapper"> 
  
  <!-- Top Bar Start -->
  <div class="topbar"> 
    
    <!-- LOGO -->
    <div class="topbar-left"> <a href="index.html" class="logo"> <img class="img-thumbnail" src="../assets/images/optimize-logo.png"> </a> </div>
    <nav class="navbar-custom">
      <ul class="navbar-right list-inline float-right mb-0">
        <li class="dropdown notification-list list-inline-item">
          <div class="dropdown notification-list nav-pro-img"> <a class="dropdown-toggle nav-link arrow-none nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"> <img src="../assets/images/companyicon.png" alt="user" class="rounded-circle"> </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown "> <a class="dropdown-item text-danger" href="#"><i class="mdi mdi-power text-danger"></i> Logout</a> </div>
          </div>
        </li>
      </ul>
      <ul class="list-inline menu-left mb-0">
        <li class="float-left mobile-view">
          <button class="button-menu-mobile open-left waves-effect"> <i class="mdi mdi-menu"></i> </button>
        </li>
      </ul>
    </nav>
  </div>
  <!-- Top Bar End --> 
  
  <!-- ========== Left Sidebar Start ========== -->
  <div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll"> 
      
      <!--- Sidemenu -->
      <div class="left side-menu">
        <div class="slimscroll-menu" id="remove-scroll"> 
          
          <!--- Sidemenu -->
          <div id="sidebar-menu"> 
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
              <li> <a href="index.html" class="waves-effect"> <i class="icon-squares"></i> <span> Dashboard </span> </a> </li>
            <li> <a href="quotation-requests.html" class="waves-effect"><i class="icon-calendar"></i><span>Quotation Requests</span></a> </li>
            <li> <a href="business-card.html" class="waves-effect"><i class="icon-calendar"></i><span>Business Card</span></a> </li>
            <li> <a href="customer-chat.html" class="waves-effect"><i class="icon-calendar"></i><span>Customer Chat</span></a> </li>
              
            </ul>
          </div>
          <!-- Sidebar -->
          <div class="clearfix"></div>
        </div>
        <!-- Sidebar -left --> 
        
      </div>
      <!-- Sidebar -->
      <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left --> 
    
  </div>
  <!-- Left Sidebar End --> 
  
  <!-- ============================================================== --> 
  <!-- Start right Content here --> 
  <!-- ============================================================== -->
  <div class="content-page"> 
    <!-- Start content -->
    <div class="content">
      <div class="container-fluid"> 
        <!-- end page-title -->
        <h3>Quotation Requests</h3>
        <div class="row">
          <div class="col-12">
            <div class="card m-b-30">
              <div class="card-body">
                <div class="mb-3" style="float:right"> <span>
                  <button type="button" class="btn btn-outline-primary waves-effect waves-light">Export in Excel</button>
                  </span> <span>
                  <button type="button" class="btn btn-outline-primary waves-effect waves-light">Export in PDF</button>
                  </span></div>
                <div style="clear:both"></div>
                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Mobile No</th>
                      <th>Subject</th>
                      <th>Message</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System@gimal.com</td>
                      <td>00000000000</td>
                      <td>sadadasd</td>
                      <td>asdasdasdasdsd</td>
                    </tr>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System@gimal.com</td>
                      <td>00000000000</td>
                      <td>sadadasd</td>
                      <td>asdasdasdasdsd</td>
                    </tr>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System@gimal.com</td>
                      <td>00000000000</td>
                      <td>sadadasd</td>
                      <td>asdasdasdasdsd</td>
                    </tr>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System@gimal.com</td>
                      <td>00000000000</td>
                      <td>sadadasd</td>
                      <td>asdasdasdasdsd</td>
                    </tr>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System@gimal.com</td>
                      <td>00000000000</td>
                      <td>sadadasd</td>
                      <td>asdasdasdasdsd</td>
                    </tr>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System@gimal.com</td>
                      <td>00000000000</td>
                      <td>sadadasd</td>
                      <td>asdasdasdasdsd</td>
                    </tr>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System@gimal.com</td>
                      <td>00000000000</td>
                      <td>sadadasd</td>
                      <td>asdasdasdasdsd</td>
                    </tr>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System@gimal.com</td>
                      <td>00000000000</td>
                      <td>sadadasd</td>
                      <td>asdasdasdasdsd</td>
                    </tr>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System@gimal.com</td>
                      <td>00000000000</td>
                      <td>sadadasd</td>
                      <td>asdasdasdasdsd</td>
                    </tr>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System@gimal.com</td>
                      <td>00000000000</td>
                      <td>sadadasd</td>
                      <td>asdasdasdasdsd</td>
                    </tr>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System@gimal.com</td>
                      <td>00000000000</td>
                      <td>sadadasd</td>
                      <td>asdasdasdasdsd</td>
                    </tr>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System@gimal.com</td>
                      <td>00000000000</td>
                      <td>sadadasd</td>
                      <td>asdasdasdasdsd</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
        
      </div>
      <!-- container-fluid --> 
      
    </div>
    <!-- content --> 
    
    <!-- content -->
    <footer class="footer"> © 2020 <span class="d-none d-sm-inline-block"> by Optimize</span>. </footer>
  </div>
  <!-- ============================================================== --> 
  <!-- End Right content here --> 
  <!-- ============================================================== --> 
  
</div>
<!-- END wrapper --> 
<!-- END wrapper --> 
@endsection
