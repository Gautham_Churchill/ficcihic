@extends('layouts.app')

@section('content')
<!-- Begin page -->
<div id="wrapper"> 
  
  <!-- Top Bar Start -->
  <div class="topbar"> 
    
    <!-- LOGO -->
    <div class="topbar-left"> <a href="index.html" class="logo"> <img class="img-thumbnail" src="../assets/images/optimize-logo.png"> </a> </div>
    <nav class="navbar-custom">
      <ul class="navbar-right list-inline float-right mb-0">
        <li class="dropdown notification-list list-inline-item">
          <div class="dropdown notification-list nav-pro-img"> <a class="dropdown-toggle nav-link arrow-none nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"> <img src="../assets/images/companyicon.png" alt="user" class="rounded-circle"> </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown "> <a class="dropdown-item text-danger" href="#"><i class="mdi mdi-power text-danger"></i> Logout</a> </div>
          </div>
        </li>
      </ul>
      <ul class="list-inline menu-left mb-0">
        <li class="float-left mobile-view">
          <button class="button-menu-mobile open-left waves-effect"> <i class="mdi mdi-menu"></i> </button>
        </li>
      </ul>
    </nav>
  </div>
  <!-- Top Bar End --> 
  
  <!-- ========== Left Sidebar Start ========== -->
  <div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll"> 
      
      <!--- Sidemenu -->
      <div class="left side-menu">
        <div class="slimscroll-menu" id="remove-scroll"> 
          
          <!--- Sidemenu -->
          <div id="sidebar-menu"> 
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
              <li> <a href="index.html" class="waves-effect"> <i class="icon-squares"></i> <span> Dashboard </span> </a> </li>
            <li> <a href="quotation-requests.html" class="waves-effect"><i class="icon-calendar"></i><span>Quotation Requests</span></a> </li>
            <li> <a href="business-card.html" class="waves-effect"><i class="icon-calendar"></i><span>Business Card</span></a> </li>
            <li> <a href="customer-chat.html" class="waves-effect"><i class="icon-calendar"></i><span>Customer Chat</span></a> </li>
              
            </ul>
          </div>
          <!-- Sidebar -->
          <div class="clearfix"></div>
        </div>
        <!-- Sidebar -left --> 
        
      </div>
      <!-- Sidebar -->
      <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left --> 
    
  </div>
  <!-- Left Sidebar End --> 
  
  <!-- ============================================================== --> 
  <!-- Start right Content here --> 
  <!-- ============================================================== -->
  <div class="content-page"> 
    <!-- Start content -->
    <div class="content">
      <div class="container-fluid"> 
        <!-- end page-title -->
        <h3>Business Card</h3>
        <div class="row">
          <div class="col-12">
            <div class="card m-b-30">
              <div class="card-body">
                <div class="mb-3" style="float:right"> <span>
                  <button type="button" class="btn btn-outline-primary waves-effect waves-light">Export in Excel</button>
                  </span> <span>
                  <button type="button" class="btn btn-outline-primary waves-effect waves-light">Export in PDF</button>
                  </span></div>
                <div style="clear:both"></div>
                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Designation </th>
                      <th>Organisation name </th>
                      <th>Email</th>
                      <th>Number</th>
                      <th>Address</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System Architect</td>
                      <td>Edinburgh</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Garrett Winters</td>
                      <td>Accountant</td>
                      <td>Tokyo</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Ashton Cox</td>
                      <td>Junior Technical Author</td>
                      <td>San Francisco</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Cedric Kelly</td>
                      <td>Senior Javascript Developer</td>
                      <td>Edinburgh</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Airi Satou</td>
                      <td>Accountant</td>
                      <td>Tokyo</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Brielle Williamson</td>
                      <td>Integration Specialist</td>
                      <td>New York</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Herrod Chandler</td>
                      <td>Sales Assistant</td>
                      <td>San Francisco</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Rhona Davidson</td>
                      <td>Integration Specialist</td>
                      <td>Tokyo</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Colleen Hurst</td>
                      <td>Javascript Developer</td>
                      <td>San Francisco</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Sonya Frost</td>
                      <td>Software Engineer</td>
                      <td>Edinburgh</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Jena Gaines</td>
                      <td>Office Manager</td>
                      <td>London</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Quinn Flynn</td>
                      <td>Support Lead</td>
                      <td>Edinburgh</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Charde Marshall</td>
                      <td>Regional Director</td>
                      <td>San Francisco</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Haley Kennedy</td>
                      <td>Senior Marketing Designer</td>
                      <td>London</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Tatyana Fitzpatrick</td>
                      <td>Regional Director</td>
                      <td>London</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Michael Silva</td>
                      <td>Marketing Designer</td>
                      <td>London</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Paul Byrd</td>
                      <td>Chief Financial Officer (CFO)</td>
                      <td>New York</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Gloria Little</td>
                      <td>Systems Administrator</td>
                      <td>New York</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Bradley Greer</td>
                      <td>Software Engineer</td>
                      <td>London</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Dai Rios</td>
                      <td>Personnel Lead</td>
                      <td>Edinburgh</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Jenette Caldwell</td>
                      <td>Development Lead</td>
                      <td>New York</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Yuri Berry</td>
                      <td>Chief Marketing Officer (CMO)</td>
                      <td>New York</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Caesar Vance</td>
                      <td>Pre-Sales Support</td>
                      <td>New York</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Doris Wilder</td>
                      <td>Sales Assistant</td>
                      <td>Sidney</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Angelica Ramos</td>
                      <td>Chief Executive Officer (CEO)</td>
                      <td>London</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Gavin Joyce</td>
                      <td>Developer</td>
                      <td>Edinburgh</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Jennifer Chang</td>
                      <td>Regional Director</td>
                      <td>Singapore</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Brenden Wagner</td>
                      <td>Software Engineer</td>
                      <td>San Francisco</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Fiona Green</td>
                      <td>Chief Operating Officer (COO)</td>
                      <td>San Francisco</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Shou Itou</td>
                      <td>Regional Marketing</td>
                      <td>Tokyo</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Michelle House</td>
                      <td>Integration Specialist</td>
                      <td>Sidney</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Suki Burks</td>
                      <td>Developer</td>
                      <td>London</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Prescott Bartlett</td>
                      <td>Technical Author</td>
                      <td>London</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Gavin Cortez</td>
                      <td>Team Leader</td>
                      <td>San Francisco</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Martena Mccray</td>
                      <td>Post-Sales support</td>
                      <td>Edinburgh</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Unity Butler</td>
                      <td>Marketing Designer</td>
                      <td>San Francisco</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Howard Hatfield</td>
                      <td>Office Manager</td>
                      <td>San Francisco</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Hope Fuentes</td>
                      <td>Secretary</td>
                      <td>San Francisco</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Vivian Harrell</td>
                      <td>Financial Controller</td>
                      <td>San Francisco</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Timothy Mooney</td>
                      <td>Office Manager</td>
                      <td>London</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Jackson Bradshaw</td>
                      <td>Director</td>
                      <td>New York</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Olivia Liang</td>
                      <td>Support Engineer</td>
                      <td>Singapore</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Bruno Nash</td>
                      <td>Software Engineer</td>
                      <td>London</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Sakura Yamamoto</td>
                      <td>Support Engineer</td>
                      <td>Tokyo</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Thor Walton</td>
                      <td>Developer</td>
                      <td>New York</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Finn Camacho</td>
                      <td>Support Engineer</td>
                      <td>San Francisco</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Serge Baldwin</td>
                      <td>Data Coordinator</td>
                      <td>Singapore</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Zenaida Frank</td>
                      <td>Software Engineer</td>
                      <td>New York</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Zorita Serrano</td>
                      <td>Software Engineer</td>
                      <td>San Francisco</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Jennifer Acosta</td>
                      <td>Junior Javascript Developer</td>
                      <td>Edinburgh</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Cara Stevens</td>
                      <td>Sales Assistant</td>
                      <td>New York</td>
                      <td>abc@gmail.com</td>
                      <td>910000000</td>
                      <td>Lado Sarai</td>
                    </tr>
                    <tr>
                      <td>Hermione Butler</td>
                      <td>Regional Director</td>
                      <td>London</td>
                      <td>47</td>
                      <td>2011/03/21</td>
                      <td>$356,250</td>
                    </tr>
                    <tr>
                      <td>Lael Greer</td>
                      <td>Systems Administrator</td>
                      <td>London</td>
                      <td>21</td>
                      <td>2009/02/27</td>
                      <td>$103,500</td>
                    </tr>
                    <tr>
                      <td>Jonas Alexander</td>
                      <td>Developer</td>
                      <td>San Francisco</td>
                      <td>30</td>
                      <td>2010/07/14</td>
                      <td>$86,500</td>
                    </tr>
                    <tr>
                      <td>Shad Decker</td>
                      <td>Regional Director</td>
                      <td>Edinburgh</td>
                      <td>51</td>
                      <td>2008/11/13</td>
                      <td>$183,000</td>
                    </tr>
                    <tr>
                      <td>Michael Bruce</td>
                      <td>Javascript Developer</td>
                      <td>Singapore</td>
                      <td>29</td>
                      <td>2011/06/27</td>
                      <td>$183,000</td>
                    </tr>
                    <tr>
                      <td>Donna Snider</td>
                      <td>Customer Support</td>
                      <td>New York</td>
                      <td>27</td>
                      <td>2011/01/25</td>
                      <td>$112,000</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
        
      </div>
      <!-- container-fluid --> 
      
    </div>
    <!-- content --> 
    
    <!-- content -->
    <footer class="footer"> © 2020 <span class="d-none d-sm-inline-block"> by Optimize</span>. </footer>
  </div>
  <!-- ============================================================== --> 
  <!-- End Right content here --> 
  <!-- ============================================================== --> 
  
</div>
<!-- END wrapper --> 
@endsection
