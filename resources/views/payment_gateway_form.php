@extends('layouts.app')

@section('content')
<div id="preloader">
  <div data-loader="circle-side"></div>
</div>
<!-- /Preload -->

<div id="loader_form">
  <div data-loader="circle-side-2"></div>
</div>
<!-- /loader_form -->

<!--<nav>
  <ul class="cd-primary-nav">
    <li><a href="#" class="animated_link">Home</a></li>
    <li><a href="#" class="animated_link">Quote Version</a></li>
    <li><a href="#" class="animated_link">Review Version</a></li>
    <li><a href="#" class="animated_link">Registration Version</a></li>
    <li><a href="#" class="animated_link">About Us</a></li>
    <li><a href="#" class="animated_link">Contact Us</a></li>
  </ul>
</nav>-->
<!-- /menu -->

<div class="container-fluid full-height">
  <div class="row row-height">
    <!-- /content-left -->
    <div class="col-lg-6 content-right" id="start">
    
    <div id="wizard_container">
    <a href="index.html" id="logo"><img src="img/optimize-logo.png" alt=""></a>
        <div id="top-wizard">
          <div id="progressbar"></div>
        </div>
        <!-- /top-wizard -->
        <form id="wrapped-x" method="POST" action="{{url('/register-user')}}" class="form_bg">
          @csrf
          <input id="website" name="website" type="text" value="">
          <!-- Leave for security protection, read docs for details -->
          <div id="middle-wizard">

            @if(Session::has('flash_message_error'))
        <div class="alert alert-sm alert-danger alert-block" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{!! session('flash_message_error') !!}</strong>
        </div>
        @endif

        @if(Session::has('flash_message_success'))
        <div class="alert alert-sm alert-success alert-block" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{!! session('flash_message_success') !!}</strong>
        </div>
        @endif

            <div class="step">
              <h3 class="main_question"><strong>1/3</strong>Registration form</h3>
              <div class="form-group">
                <input type="text" name="first_name" class="form-control required" placeholder="First Name" onchange="getVals(this, 'first_name');">
              </div>
              <div class="form-group">
                <input type="text" name="last_name" class="form-control required" placeholder="Last Name" onchange="getVals(this, 'last_name');">
              </div>
              <div class="form-group">
                <input type="text" name="desgination" class="form-control required" placeholder="Designation" onchange="getVals(this, 'designation');">
              </div>
              <div class="form-group">
                <input type="email" name="email" class="form-control required" placeholder="Your Email" onchange="getVals(this, 'email');">
              </div>
              <div class="form-group">
                <input type="text" name="mobile" class="form-control required" placeholder="Mobile" onchange="getVals(this, 'mobile');">
              </div>
    
              <div class="form-group terms">
                <label class="container_check">Please accept our <a href="#" data-toggle="modal" data-target="#terms-txt">Terms and conditions</a>
                  <input type="checkbox" name="terms" value="Yes" class="required">
                  <span class="checkmark"></span> </label>
              </div>
            </div>
            <!-- /step-->
            <div class="step">
              <h3 class="main_question"><strong>2/3</strong>Organisation details</h3>
              <div class="form-group">
                <input type="text" name="name_of_organisation" class="form-control required" placeholder="Name of organisation" onchange="getVals(this, 'user_name');">
              </div>
              <div class="form-group">
                <input class="form-control required" name="address" type="text" placeholder="Address">
              </div>
              <div class="row">
                <div class="col-6">
                <p class="mt-2">Do you have GST no. </p>
                </div>
                <div class="col-6">
                  <div class="form-group radio_input">
                    <label class="container_radio">Yes
                      <input type="radio" name="gst" value="Yes" class="required">
                      <span class="checkmark"></span> </label>
                    <label class="container_radio">No
                      <input type="radio" name="gst" value="No" class="required">
                      <span class="checkmark"></span> </label>
                  </div>
                </div>
                <div class="col-5"><input type="text" id="verify_contact" name="gst_no" class=" form-control" placeholder="GST no"></div>
              </div>
              <div id="pass-info" class="clearfix"></div>
            </div>
            <!-- /step-->
            <div class="submit step">
              <h3 class="main_question"><strong>3/3</strong>Payment Gateway</h3>
              
            </div>
            <!-- /step--> 
          </div>
          <!-- /middle-wizard -->
          <div id="bottom-wizard">
            <button type="button" name="backward" class="backward">Prev</button>
            <button type="button" name="forward" class="forward">Next</button>
            <button type="submit" name="process" class="submit">Submit</button>
          </div>
          <!-- /bottom-wizard -->
        </form>
      </div>
      <!-- /Wizard container --> 
    </div>
    <!-- /content-right-->
    <div class="col-lg-6 content-left">
      <div class="content-left-wrapper"> 
      
        <div>
          <figure><img src="img/HIC-logo.png" alt="" class="img-fluid"></figure>
          <h2>I have already registered to<br>the event</h2>
          <p>In the next step you can sign in to or create a new account</p>
          <a href="/login" class="btn_1">Login</a><!-- <a href="#start" class="btn_1 rounded mobile_btn">Start Now!</a> --></div>
        <div class="copy">© 2020 Optimize</div>
      </div>
      <!-- /content-left-wrapper --> 
    </div>
    
    
    
    
     
  </div>
  <!-- /row--> 
</div>
<!-- /container-fluid -->

<div class="cd-overlay-nav"> <span></span> </div>
<!-- /cd-overlay-nav -->

<div class="cd-overlay-content"> <span></span> </div>
<!-- /cd-overlay-content --> 

<!--<a href="#0" class="cd-nav-trigger">Menu<span class="cd-icon"></span></a>--> 
<!-- /menu button --> 

<!-- Modal terms -->
<div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="termsLabel">Terms and conditions</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <p>Lorem ipsum dolor sit amet, in porro albucius qui, in <strong>nec quod novum accumsan</strong>, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>
        <p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus. Lorem ipsum dolor sit amet, <strong>in porro albucius qui</strong>, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>
        <p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn_1" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
