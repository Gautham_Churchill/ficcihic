<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Wilio Survey, Quotation, Review and Register form Wizard by Ansonika.">
<meta name="author" content="Ansonika">
<title>Login</title>

<!-- Favicons-->
<link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
<!--<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">-->
<link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

<!-- GOOGLE WEB FONT -->
<link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.check-con{ background:#26ae88; padding:9px; width:60px; height:60px; margin:0 auto;border-radius:50%; margin-bottom:10px;}
.check-con i{ padding:0; font-size:32px; font-weight:100 !important; color:#fff;}
.cancel-con{ background:#de061e; padding:9px; width:60px; height:60px; margin:0 auto;border-radius:50%; margin-bottom:10px;}
.cancel-con i{ padding:0; font-size:32px; font-weight:100 !important; color:#fff;}
.msg h3{font-size: 1.6rem; margin-bottom:14px;}
.msg p{font-size: 0.9rem;margin-bottom:10px;}
.msg p strong{font-size: 1.0rem;}
.msg p b{font-size: 1.0rem; color:#000;}
.msg-bor{border-top: 1px solid #26ae88; width:80%; margin:0 auto; margin-top:18px; margin-bottom:18px;}
.msg-bottom{ background: #26ae88; padding:10px !important;border-radius:0 0 10px 10px; color:#fff;}
.social a{ text-decoration:none !important;}
.fa {
  padding: 8px;
  font-size: 19px;
  width: 36px;
  height:36px;
  text-align: center;
  text-decoration: none;
  margin: 5px 4px;
  border-radius:50%;
}

.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-google {
  background: #dd4b39;
  color: white;
}

.fa-linkedin {
  background: #007bb5;
  color: white;
}

.fa-youtube {
  background: #bb0000;
  color: white;
}

.fa-instagram {
  background: #125688;
  color: white;
}

.fa-pinterest {
  background: #cb2027;
  color: white;
}

.fa-snapchat-ghost {
  background: #fffc00;
  color: white;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

.fa-skype {
  background: #00aff0;
  color: white;
}

.fa-android {
  background: #a4c639;
  color: white;
}

.fa-dribbble {
  background: #ea4c89;
  color: white;
}

.fa-vimeo {
  background: #45bbff;
  color: white;
}

.fa-tumblr {
  background: #2c4762;
  color: white;
}

.fa-vine {
  background: #00b489;
  color: white;
}

.fa-foursquare {
  background: #45bbff;
  color: white;
}

.fa-stumbleupon {
  background: #eb4924;
  color: white;
}

.fa-flickr {
  background: #f40083;
  color: white;
}

.fa-yahoo {
  background: #430297;
  color: white;
}

.fa-soundcloud {
  background: #ff5500;
  color: white;
}

.fa-reddit {
  background: #ff5700;
  color: white;
}

.fa-rss {
  background: #ff6600;
  color: white;
}

.fa-envelope {
  background: #dd4b39;
  color: white;
}
</style>


<!-- BASE CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/menu.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/vendors.css" rel="stylesheet">

<!-- YOUR CUSTOM CSS -->
<link href="css/custom.css" rel="stylesheet">

<!-- MODERNIZR MENU -->
<script src="js/modernizr.js"></script>
</head>

<body class="forgot-bg">
<div id="preloader">
  <div data-loader="circle-side"></div>
</div>
<!-- /Preload -->

<div id="loader_form">
  <div data-loader="circle-side-2"></div>
</div>
<!-- /loader_form -->
<!-- /menu -->
<div class="container-fluid full-height">
  <div class="row row-height">
    <!-- /content-left -->
    <div class="col-lg-12 content-right" id="start">
    <!---->
    
    <!-- /social -->
      <div id="wizard_container2">
      <a href="/" id="logo"><img src="img/optimize-logolt.png" alt=""></a>
      
        <div id="top-wizard">
          <div id="progressbar"></div>
        </div>
        <!-- /top-wizard -->
        <form id="wrapped" method="POST" class="form_bg">
          <input id="website" name="website" type="text" value="">
          <!-- Leave for security protection, read docs for details -->
          <div id="middle-wizard">
            <div class="step">
              <div style="text-align:center;"><div class="cancel-con"><i class="fa fa-times"></i></div></div>
              <div class="msg">
              <h3>Payment Failed!</h3>
              <p><b>Your transaction has been failed to process.</b></p>
              <p><strong>Please pay again.</strong></p>
              </div>
            </div>
            <!-- /step-->
          </div>
          <!-- /middle-wizard -->
          <div id="bottom-wizard" class=" msg-bottom" style="text-align:center; margin-top: 14px;">
          <div class="msg">
          <p>We are happy to have you, using our platform<br> 
             Best, Team Optimise </p>
          </div>
          </div>
          
          <!-- /bottom-wizard -->
        </form>
      </div>
      <!-- /Wizard container --> 
    </div>
    <!-- /content-right-->
    
    
     
  </div>
  <!-- /row--> 
</div>
<!-- /container-fluid -->

<div class="cd-overlay-nav"> <span></span> </div>
<!-- /cd-overlay-nav -->

<div class="cd-overlay-content"> <span></span> </div>
<!-- /cd-overlay-content --> 

<!--<a href="#0" class="cd-nav-trigger">Menu<span class="cd-icon"></span></a>--> 
<!-- /menu button --> 

<!-- Modal terms -->
<!-- /.modal --> 

<!-- COMMON SCRIPTS --> 
<script src="js/jquery-3.5.1.min.js"></script> 
<script src="js/common_scripts.min.js"></script> 
<script src="js/velocity.min.js"></script> 
<script src="js/functions.js"></script> 
<script src="js/pw_strenght.js"></script> 

<!-- Wizard script --> 
<script src="js/registration_func.js"></script>
</body>
</html>