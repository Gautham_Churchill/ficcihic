@extends('layouts.app')

@section('content')
<div id="preloader">
  <div data-loader="circle-side"></div>
</div>
<!-- /Preload -->

<div id="loader_form">
  <div data-loader="circle-side-2"></div>
</div>
<!-- /loader_form -->

<div class="container-fluid full-height">
  <div class="row row-height">
    <!-- /content-left -->
    <div class="col-lg-6 content-right" id="start">
    
    <!-- /social -->
      <div id="wizard_container">
      <a href="/" id="logo"><img src="{{ asset('img/optimize-logodrk.png') }}" alt=""></a>
        <div id="top-wizard">
          <div id="progressbar"></div>
        </div>
        <!-- /top-wizard -->


        <form id="wrapped-x" method="POST" action="{{url('/login-user')}}" enctype="multipart/form-data" class="form_bg">
          @csrf

<!-- >>>>>>> dd4d1c2ac52cc9929248a7e38e501231f1df2f60 -->
          <input id="website" name="website" type="text" value="">
          <!-- Leave for security protection, read docs for details -->
          <div id="middle-wizard">
            <div class="step">

              @if(Session::has('flash_message_error'))
        <div class="alert alert-sm alert-danger alert-block" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{!! session('flash_message_error') !!}</strong>
        </div>
        @endif

        @if(Session::has('flash_message_success'))
        <div class="alert alert-sm alert-success alert-block" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{!! session('flash_message_success') !!}</strong>
        </div>
        @endif



              <h3 class="main_question">Login</h3>
              <div class="form-group">
                <input type="text" name="email" class="form-control required" placeholder="Email id" onchange="getVals(this, 'user_name');">
              </div>
              <div class="form-group">
                <input class="form-control required" type="password" id="password1" name="password" placeholder="Event Code" onchange="getVals(this, 'password');">
              </div>
              <div class="form-group terms">
              <a href="{{url('/forgot')}}">Forgot Code</a>
              </div>
            </div>
            <!-- /step-->
          </div>

          <!-- /middle-wizard -->
<!-- <<<<<<< HEAD
=======
 -->
<!-- >>>>>>> dd4d1c2ac52cc9929248a7e38e501231f1df2f60 -->
          <div id="bottom-wizard" style="text-align:center;">  
            
            <!-- <button type="submit" name="submit" class="submit" style=" display:inline-block">Login</button> -->

            <input type="submit" class="btn btn-primary" name="submit" value="Login">

            
            
          </div>


          <!-- <div style="text-align:center; margin-top:25px;">
          <p style="margin-bottom:10px;">Or</p>
          <a href="{{url('/login/facebook')}}" class="fa fa-facebook"></a>
          <a href="#" class="fa fa-twitter"></a>
          <a href="{{url('/login/google')}}" class="fa fa-google"></a>
          <a href="{{url('/login/linkedin')}}" class="fa fa-linkedin"></a>
          </div> -->
          <!-- /bottom-wizard -->
        </form>
      </div>
      <!-- /Wizard container --> 
    </div>
    <!-- /content-right-->
    <div class="col-lg-6 content-left">
      <div class="content-left-wrapper"> 
      
        <div>
          <figure><img src="{{ asset('img/HIC-logo.png') }}" alt="" class="img-fluid"></figure>
          <h2>Not Registered on yet</h2>
          
          <a href="/register" class="btn_1">Register Now</a></div>
        <div class="copy">© 2020 Optimize</div>
      </div>
      <!-- /content-left-wrapper --> 
    </div>
     
  </div>
  <!-- /row--> 
</div>
<!-- /container-fluid -->

<div class="cd-overlay-nav"> <span></span> </div>
<!-- /cd-overlay-nav -->

<div class="cd-overlay-content"> <span></span> </div>
<!-- /cd-overlay-content --> 
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
