@extends('layouts.app')
<style type="text/css">
  .extleft{text-align: left;}
</style>
@section('content')
<div id="preloader">
  <div data-loader="circle-side"></div>
</div>
<!-- /Preload -->

<div id="loader_form">
  <div data-loader="circle-side-2"></div>
</div>
<!-- /loader_form -->

<!--<nav>
  <ul class="cd-primary-nav">
    <li><a href="#" class="animated_link">Home</a></li>
    <li><a href="#" class="animated_link">Quote Version</a></li>
    <li><a href="#" class="animated_link">Review Version</a></li>
    <li><a href="#" class="animated_link">Registration Version</a></li>
    <li><a href="#" class="animated_link">About Us</a></li>
    <li><a href="#" class="animated_link">Contact Us</a></li>
  </ul>
</nav>-->
<!-- /menu -->

<div class="container-fluid full-height">
  <div class="row row-height">
    <!-- /content-left -->
    <div class="col-lg-6 content-right" id="start">
    
    <div id="wizard_container">
    <a href="/" id="logo"><img src="{{ asset('img/optimize-logodrk.png') }}" alt=""></a>
        <div id="top-wizard">
          <div id="progressbar"></div>
        </div>
        <!-- /top-wizard -->
        <form id="wrapped-x" method="POST" action="http://payment.ficci.com/fvhico/paymentprocess.asp" class="form_bg" >
         
          <input id="website" name="website" type="text" value="">
          <!-- Leave for security protection, read docs for details -->
          <div id="middle-wizard">

            
        <div class="alert alert-sm alert-danger alert-block" role="alert" style="display: none;">
            <strong></strong>
        </div>
        

        @if(Session::has('flash_message_success'))
        <div class="alert alert-sm alert-success alert-block" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{!! session('flash_message_success') !!}</strong>
        </div>
        @endif

            <div class="step" data-id="1">
              <h3 class="main_question"><strong>1/3</strong>Registration form</h3>
              <div class="form-group extleft" >
                <input type="text" id="form_fname" name="ContactName" class="form-control required" placeholder="Name" onchange="getVals(this, 'first_name');">
                <input type="text" id="RegId" value="" name="RegId" class="form-control" hidden>
                <input type="text" id="Amount" value="" name="Amount" class="form-control" hidden>
                <input type="text" id="quantity" value="1" name="quantity" class="form-control" hidden>
                <input type="text" id="City" value="na" name="City" class="form-control" hidden>
                <input type="text" id="country" value="india" name="country" class="form-control" hidden>
                <input type="text" id="pageurl" value="http://ficcihic.optimizevents.com/paymentUser" name="pageurl" class="form-control" hidden>
                <span class="error_form" id="fname_error_message"></span>
              </div>
              

              <div class="form-group extleft">
                <input type="text" id="form_designation" name="Designation" class="form-control required" placeholder="Designation" onchange="getVals(this, 'designation');">
                <span class="error_form" id="designation_error_message"></span>
              </div>
              

              <div class="form-group extleft">
                <input type="email" id="form_email" name="OfficialEmail" class="form-control required" placeholder="Your Email" onchange="getVals(this, 'email');">
                <span class="error_form" id="email_error_message"></span>
              </div>
              
              
              <div class="form-group extleft">
                <input type="text" id="form_mobile" name="mobile" class="form-control required" placeholder="Mobile" onchange="getVals(this, 'mobile');">
                <span class="error_form" id="mobile_error_message"></span>
              </div>
              

               <div class="form-group add_top_10" style="text-align:left;">
                <label><strong>Linkedin Profile</strong></label>
                <input type="text" name="linkedin_link" class="form-control" placeholder="Link" onchange="getVals(this, 'mobile');">
              </div>

              <div class="form-group add_top_10" style="text-align:left;">
                <label><strong>Twitter Profile</strong></label>
                <input type="text" name="twitter_link" class="form-control" placeholder="Link" onchange="getVals(this, 'mobile');">
              </div>

              <div class="form-group add_top_10 extleft" style="text-align:left;">
                <label><strong>Choose Display Picture</strong><br>
                  <small>(Files accepted: jpg, jpeg - Max file size: 500px * 500px )</small></label>
                
                <div class="fileupload">
                  <input type="file" name="image" id="image" accept="image/*,.pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" onchange="getVals(this, 'fileupload');">
                  <span class="error_form" id="file_error_message"></span>
                </div>
              </div>
    
              <div class="form-group terms extleft">
                <label class="container_check">Please accept our <a href="#" data-toggle="modal" data-target="#terms-txt">Terms and conditions</a>
                  <input type="checkbox" name="terms" value="Yes" class="required">
                  <span class="checkmark"></span> </label>
              </div>
            </div>
            <!-- /step-->
            <div class="step" data-id="2">
              <h3 class="main_question"><strong>2/3</strong>Organisation details</h3>
              <div class="form-group extleft">
                <input type="text" id="form_org" name="Organisation" class="form-control required" placeholder="Name of organisation" onchange="getVals(this, 'user_name');">

                <span class="error_form extleft" id="org_error_message"></span>
              </div>
              <div class="form-group extleft">
                <input class="form-control required" id="form_address" name="CompanyAddress" type="text" placeholder="Address">
                <span class="error_form" id="address_error_message"></span>
              </div>
              <div class="form-group extleft">
                <select class="form-control required" id="form_state" name="State" type="text" placeholder="State">
                  <option value="">Select Your State</option>
                  <option value="ANDAMAN AND NICOBAR ISLANDS">ANDAMAN AND NICOBAR ISLANDS</option>
                  <option value="ANDHRA PRADESH">ANDHRA PRADESH</option>
                  <option value="ARUNACHAL PRADESH">ARUNACHAL PRADESH</option>
                  <option value="ASSAM">ASSAM</option>
                  <option value="BIHAR">BIHAR</option>
                  <option value="CHATTISGARH">CHATTISGARH</option>
                  <option value="CHANDIGARH">CHANDIGARH</option>
                  <option value="DAMAN AND DIU">DAMAN AND DIU</option>
                  <option value="DELHI">DELHI</option>
                  <option value="DADRA AND NAGAR HAVELI">DADRA AND NAGAR HAVELI</option>
                  <option value="GOA">GOA</option>
                  <option value="GUJARAT">GUJARAT</option>
                  <option value="HIMACHAL PRADESH">HIMACHAL PRADESH</option>
                  <option value="HARYANA">HARYANA</option>
                  <option value="JAMMU AND KASHMIR">JAMMU AND KASHMIR</option>
                  <option value="JHARKHAND">JHARKHAND</option>
                  <option value="KERALA">KERALA</option>
                  <option value="KARNATAKA">KARNATAKA</option>
                  <option value="LAKSHADWEEP">LAKSHADWEEP</option>
                  <option value="MEGHALAYA">MEGHALAYA</option>
                  <option value="MAHARASHTRA">MAHARASHTRA</option>
                  <option value="MANIPUR">MANIPUR</option>
                  <option value="MADHYA PRADESH">MADHYA PRADESH</option>
                  <option value="MIZORAM">MIZORAM</option>
                  <option value="NAGALAND">NAGALAND</option>
                  <option value="ORISSA">ORISSA</option>
                  <option value="PUNJAB">PUNJAB</option>
                  <option value="PONDICHERRY">PONDICHERRY</option>
                  <option value="RAJASTHAN">RAJASTHAN</option>
                  <option value="SIKKIM">SIKKIM</option>
                  <option value="TAMIL NADU">TAMIL NADU</option>
                  <option value="TRIPURA">TRIPURA</option>
                  <option value="UTTARAKHAND">UTTARAKHAND</option>
                  <option value="UTTAR PRADESH">UTTAR PRADESH</option>
                  <option value="WEST BENGAL">WEST BENGAL</option>
                  <option value="TELANGANA">TELANGANA</option>

                </select>
                <span class="error_form" id="state_error_message"></span>
              </div>
              <div class="form-group extleft">
                <input class="form-control required" id="form_zipcode" name="ZipCode" type="text" placeholder="ZipCode">
                <span class="error_form" id="zipcode_error_message"></span>
              </div>
              <div class="form-group extleft">
                <input class="form-control required" id="phone" name="phone" type="text" placeholder="Phone">
                <span class="error_form" id="phone_error_message"></span>
              </div>
              

              
              <div class="row">
                <div class="col-7">
                <p class="mt-2 mb-0"><strong>Are you a FICCI member?</strong></p>
                <span class="error_form" id="ficci_option_error_message"></span>
                </div>
                <div class="col-5">
                  <div class="form-group radio_input mb-2">
                    <label class="container_radio">Yes
                      <input type="radio" class="check-male" name="ficci_member" value="Yes" required="">
                      <span class="checkmark"></span> </label>
                    <label class="container_radio">No
                      <input type="radio" class="check-female" name="ficci_member" value="No" required="">
                      <span class="checkmark"></span> </label>

                  </div>
                </div>
                <div class="col-12"><input type="text" id="form_ficci" name="ficci_no" id="verify_contact" class=" form-control" placeholder="FICCI Membership Number" style="display: none" ></div>
                <span class="error_form" id="ficci_error_message"></span>
              </div>
              <div class="row">
                <div class="col-7">
                <p class="mt-2 mb-0"><strong>Do you have gst no.?</strong></p>
                <span class="error_form" id="gst_option_error_message"></span>
                </div>
                <div class="col-5">
                  <div class="form-group radio_input mb-2">
                    <label class="container_radio">Yes
                      <input type="radio" id="chkYes" name="gst" value="Yes" required="">
                      <span class="checkmark"></span> </label>
                    <label class="container_radio">No
                      <input type="radio" id="chkNo" name="gst" value="No" required="">
                      <span class="checkmark"></span> </label>
                      <span class="error_form" id="gst_option_error_message"></span>
                  </div>
                </div>
                <div class="col-12"><input type="text" id="form_gst" style="display: none" name="GSTNumber" id="verify_contact" class=" form-control" placeholder="gst Number"></div>
                <span class="error_form" id="gst_error_message"></span>
              </div>
              <div id="pass-info" class="clearfix"></div>
            </div>
            <!-- /step-->
            <div class="submit step" data-id="3">
            <h3 class="main_question"><strong>3/3</strong>Delegate Fee for Conference<br><span style="font-size:12px;">(per delegate)*</span></h3>
              <div class="form-group mb-2" style="text-align:left;">
                <label class="container_radio"><strong>FICCI Members - INR 3000 + GST</strong>
                  <input type="radio" name="payment_amount" id="ficci_radio_id" value="ficci" class="required" required="">
                  <span class="checkmark"></span> </label>
              </div>
              <div class="form-group mb-2" style="text-align:left;">
                <label class="container_radio"><strong>Non-FICCI Members - INR 3500 + GST</strong>
                <input type="radio" name="payment_amount" id="nonficci_radio_id" value="nonficci" class="required" required="">
                  <span class="checkmark"></span> </label>
              </div>
              <div class="form-group extleft" style="text-align:left;">
                <label class="container_radio"><strong>Academia - INR 750 + GST</strong>
                <input type="radio" name="payment_amount" id="academia_radio_id" value="academia" class="required" required="">
                  <span class="checkmark"></span> </label>
                  <span class="error_form" id="payment_amount_error_message"></span>
              </div>
              
              <div class="form-group add_top_10 mb-2" style="text-align:left;">
                <label><strong>Payment Options:</strong></label>
              </div>
              <div class="form-group mb-2" style="text-align:left;">
                <label class="container_radio"><strong>Online Payment</strong>
                <input type="radio" name="payment_type" value="online" class="payment_type required" required="">
                  <span class="checkmark"></span> </label>
              </div>
              <div class="form-group extleft" style="text-align:left;">
                <label class="container_radio"><strong>NEFT/ RTGS/ IMPS</strong>
                <input type="radio" name="payment_type" value="neft" class="payment_type required" required="">
                  <span class="checkmark"></span> </label>
                  <span class="error_form" id="payment_type_error_message"></span>
              </div>
              
              <div class="form-group paymentdetails_div" style="text-align:left;border: 1px solid #e0e0e0; padding:5px;border-radius: 6px; background:#f9f9f9; display: none">
              <p class="mb-1">Bank Name – Yes Bank</p>
              <p class="mb-1">Branch - 56 Janpath, Alps Building, Connaught Place, New Delhi 110001</p>
              <p class="mb-1">A/C No. – 013694600000041</p>
              <p class="mb-1">A/C Holder – Federation of Indian Chambers of Commerce and Industry</p>
              <p class="mb-1">A/C type – Saving</p>
              <p class="mb-1">IFSC Code – YESB0000136</p>
              </div>
              <div class="form-group" style="text-align:left;">
              <p class="mb-1" style="font-size:12px;">*The above participation fees is exclusive of GST of 18%.</p>
              <p class="mb-1" style="font-size:12px;">*There is a group discount of 10% for 3 or more participants from the same organization.</p>
              </div>
            </div>
            <!-- /step--> 
          </div>
          <!-- /middle-wizard -->
          <div id="bottom-wizard">
            <button type="button" name="backward" class="backward">Prev</button>
            <button type="button" name="forward" class="forward">Next</button>
            <button type="button" name="process" id="paymentpro" class="submit">Submit</button>
          </div>
          <!-- /bottom-wizard -->
        </form>
      </div>
      <!-- /Wizard container --> 
    </div>
    <!-- /content-right-->
    <div class="col-lg-6 content-left">
      <div class="content-left-wrapper"> 
      
        <div>
          <figure><img src="{{ asset('img/HIC-logo.png') }}" alt="" class="img-fluid"></figure>
          <h2>I have already registered to<br>the event</h2>
          <p>In the next step you can sign in to or create a new account</p>
          <a href="/login" class="btn_1">Login</a><!-- <a href="#start" class="btn_1 rounded mobile_btn">Start Now!</a> --></div>
        <div class="copy">© 2020 Optimize</div>
      </div>
      <!-- /content-left-wrapper --> 
    </div>
    
    
    
    
     
  </div>
  <!-- /row--> 
</div>
<!-- /container-fluid -->

<div class="cd-overlay-nav"> <span></span> </div>
<!-- /cd-overlay-nav -->

<div class="cd-overlay-content"> <span></span> </div>
<!-- /cd-overlay-content --> 

<!--<a href="#0" class="cd-nav-trigger">Menu<span class="cd-icon"></span></a>--> 
<!-- /menu button --> 

<!-- Modal terms -->
<div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-centered" style="width: 1200px;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="termsLabel">Terms and conditions</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <h6 class="modal-title" id="termsLabel">1. Software Services and Professional Services </h6>
        <h6 class="modal-title" id="termsLabel">A. Services.  </h6>
        <p>Subject to the terms, conditions, and restrictions stated in this Optimize Terms of Service (these “Terms of Service”) and each Statement of Work signed by the parties (each an “SOW”), Optimize shall provide Customer and its authorized users with web-based access to a virtual environment platform that includes the features and functionality specified in the SOW (the “Software Services”) and with migration, integration, configuration, testing, consulting and other services related to the Software Services and described in the SOW (the “Professional Services”). The Software Services, as implemented in accordance with the Professional Services, is referred to below as the “Virtual Environment Platform.” These Terms of Service and the SOW are referred to collectively as the “Agreement” below. </p>

        <ul>
  <li>User Data, Content and Security.
    <ul>
      <li>User Data. “User Data” is personal data or information collected by Optimize from or about Customer’s staff members, event attendees, and other users of the Virtual Environment Platform. Optimize shall use the User Data only to provide the Virtual Environment Platform and Professional Services, as instructed by Customer, or required by applicable law. Optimize will comply with Customer’s reasonable requests for assistance in connection with a user’s request to delete, correct, block or take other action with respect to the User Data (a “User Data Request”). Optimize shall promptly forward to Customer any User Data Request made directly on Optimize. </li>
      <li>Content. “Content” is the information and materials provided or submitted by or on behalf of Customer or its users for publication on the Virtual Environment Platform for use by Customer’s registered end users. Customer licenses the Content to Optimize on a non-exclusive, revocable basis only as necessary to provide the Services, and only for the Term of the SOW. Other than this license, as between Optimize and Customer, Customer retains all right, title and interest in and to the Content, including any modifications to or derivatives of the Content that may be prepared by Optimize as part of Professional Services. The Content and the User Data are referred to below as the “Customer Information”). Customer represents and warrants that it has all rights necessary to provide and license the Customer Information to Optimize for use as permitted by these Terms of Service and the SOW, that Optimize’ use of the Content as permitted by these Terms of Service and the SOW will not infringe, misappropriate or violate intellectual property rights of any third party, and that Optimize use of the User Data as permitted by this Agreement will not violate any rights of publicity or privacy of any third party. </li>
      <li>Security. Optimize shall put in place and maintain reasonable administrative, physical, and technical security measures to protect against unauthorized access, alteration, disclosure, and destruction of Customer Information. Customer acknowledges that the security of information transmitted over the Internet is not within Optimize full control and that Optimize cannot and does not guaranty impenetrable security of the Customer Information. </li>
      <li>Sensitive Personal Information. Customer acknowledges that the Virtual Environment Platform is not designed for processing Sensitive Personal Information, as defined below, and hereby releases Optimize from any claim or liability arising from Customer’s or its users’ transmission of Sensitive Personal Information to or through the Virtual Environment Platform. “Sensitive Personal Information” is date of birth, Social Security number, driver’s license number, other government-issued identification number, financial account number, credit or debit card number, insurance ID or account number, health or medical information, consumer reports, background checks, biometric data, digital signatures, any code or password that could be used to gain access to financial resources, any other unique identifier, or other personal data for which special protections are required by applicable privacy law </li>
      <li>Sub-processors. Optimize shall not permit sub-processors to have access to the Customer Information other than those sub-processors approved in advance in writing by Customer. Customer hereby approves the following sub- processors: Amazon Web Services, Inc., Enseur, Inc. Optimize shall require each sub-processor to contractually agree to privacy and security obligations for the protection of Customer Information at least as protective as those stated in these Terms of Service. Optimize is responsible for the acts and omissions of sub-processors in violation of these Terms of Service or the SOW to the same extent as for its own acts and omissions. </li>
      <li>Notice of Data Breach. If Optimize discovers a Security Breach, as defined below, it shall notify Customer without undue delay and in all events within seventy-two (72) hours, and provide to Customer all information reasonably available Optimize regarding the Security Breach. A “Security Breach” is an unauthorized access, alteration, disclosure, or destruction of User Data. Optimize shall promptly take those steps within its reasonable control to address a security vulnerability giving rise to a Security Breach, both to stop an ongoing Security Breach and prevent a re-occurrence due to the same security vulnerability. Optimize shall cooperate with Customer’s reasonable requests in connection with the investigation, mitigation and remediation of any Security Breach. </li>
    </ul>
  </li>
</ul>
<h6 class="modal-title" id="termsLabel">2. Grant of Licenses  </h6>
        <h6 class="modal-title" id="termsLabel">A. Grant of License to Virtual Environment Platform.  </h6>    

 <p>Subject to the terms, conditions, and restrictions stated in these Terms of Service and the SOW, Optimize hereby grants to Customer a non-exclusive license to use the Virtual Environment Platform for its internal business purposes. The license continues for the Term of the SOW, but is automatically terminated by a termination of the SOW prior to expiration. The license is non-transferable except as part of an assignment of an SOW as permitted by these Terms of Service. The license is worldwide, subject to applicable export laws. The license is conditional on Customer’s payment of the Fees. Except for the licenses expressly stated in this Section, Optimize retains all right, title and interest in and to the Virtual Environment Platform and related rights in intellectual property, including all related patents, patent applications, copyrights, trademarks, service marks, trade names, domain name rights, know-how, and trade secret rights. </p>
     <p>2. License Restrictions. Customer shall not, and shall not knowingly allow any third party to: (i) copy, use, or mirror for use all or any part of the Virtual Environment Platform on any site or system other than the Optimize service environment, (ii) modify, translate, or otherwise create derivative works of the Virtual Environment Platform, except customizations of the visual elements templates made by means of the customization features of the Virtual Environment Platform and used on the Virtual Environment Platform, (iii) disassemble, decompile, reverse engineer, or otherwise attempt to derive the source code of the Virtual Environment Platform, except and only to the extent that such activities are expressly permitted by applicable law notwithstanding this limitation, or (iv) distribute, sublicense, lease, resell or otherwise transfer rights to the Virtual Environment Platform to a third party. </p>
     <p>3. Optimize Trademark(s). Customer consents to the display of the Optimize name and “powered by Optimize” inside the Virtual Environment Platform during events and agrees that any goodwill arising from the use of Optimize’ trademarks inures exclusively to the benefit of Optimize. </p>

<h6 class="modal-title" id="termsLabel">Force Majeure.</h6> Any delay in the performance of any duties or obligations of either party (except the payment of money owed) will not be considered a breach of this Agreement if such delay is caused by a labor dispute, shortage of materials, fire, earthquake, flood, or any other event beyond such party’s reasonable control, provided that such party uses reasonable efforts, under the circumstances, to notify the other party of the circumstances causing the delay and to resume performance as soon as possible. </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn_1" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

@endsection
