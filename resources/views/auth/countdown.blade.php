@extends('layouts.app')
<link rel="stylesheet" type="text/css" href="{{ asset('countdowntime/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('countdowntime/util.css') }}">

    <!-- YOUR CUSTOM CSS -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <!-- MODERNIZR MENU -->
    <script src="{{ asset('js/modernizr.js') }}"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Sweet Alert -->
     <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js">

     <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js">
     
    <style>
    .fa {
      padding: 7px;
      font-size: 22px;
      width: 36px;
      height:36px;
      text-align: center;
      text-decoration: none;
      margin: 5px 4px;
      border-radius:50%;
    }

    .fa:hover {
        opacity: 0.7;
    }

    .fa-facebook {
      background: #3B5998;
      color: white;
    }

    .fa-twitter {
      background: #55ACEE;
      color: white;
    }

    .fa-google {
      background: #dd4b39;
      color: white;
    }

    .fa-linkedin {
      background: #007bb5;
      color: white;
    }

    .fa-youtube {
      background: #bb0000;
      color: white;
    }

    .fa-instagram {
      background: #125688;
      color: white;
    }

    .fa-pinterest {
      background: #cb2027;
      color: white;
    }

    .fa-snapchat-ghost {
      background: #fffc00;
      color: white;
      text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
    }

    .fa-skype {
      background: #00aff0;
      color: white;
    }

    .fa-android {
      background: #a4c639;
      color: white;
    }

    .fa-dribbble {
      background: #ea4c89;
      color: white;
    }

    .fa-vimeo {
      background: #45bbff;
      color: white;
    }

    .fa-tumblr {
      background: #2c4762;
      color: white;
    }

    .fa-vine {
      background: #00b489;
      color: white;
    }

    .fa-foursquare {
      background: #45bbff;
      color: white;
    }

    .fa-stumbleupon {
      background: #eb4924;
      color: white;
    }

    .fa-flickr {
      background: #f40083;
      color: white;
    }

    .fa-yahoo {
      background: #430297;
      color: white;
    }

    .fa-soundcloud {
      background: #ff5500;
      color: white;
    }

    .fa-reddit {
      background: #ff5700;
      color: white;
    }

    .fa-rss {
      background: #ff6600;
      color: white;
    }
    </style>
@section('content')
<div id="preloader">
  <div data-loader="circle-side"></div>
</div>
<!-- /Preload -->

<div id="loader_form">
  <div data-loader="circle-side-2"></div>
</div>
<!-- /loader_form -->
<!-- /menu -->
<div class="bg-img1 overlay1 size1 flex-w flex-c-m p-t-55 p-b-55 p-l-15 p-r-15" style="background-image: url('img/bg01.jpg');">
  <div class="wsize1">
    <p class="txt-center p-b-23"> <img src="img/HIC-logo.png" width="250"></p>
    <h3 class="top-head txt-center p-b-22">We are currently working on getting you the Best Virtual Event Experience. Stay tuned.</h3>
    <p class="txt-center m2-txt2 p-b-30">We will share updates on your registered e-mail.</p>
    <div class="flex-w flex-sa-m cd100 bor1 p-t-32 p-b-22 p-l-50 p-r-50 respon1" style="border-radius:10px;">
      <div class="flex-col-c-m wsize2 m-b-20"> <span class="l1-txt2 p-b-4 days">35</span> <span class="m2-txt2">Days</span> </div>
      <span class="l1-txt2 p-b-22">:</span>
      <div class="flex-col-c-m wsize2 m-b-20"> <span class="l1-txt2 p-b-4 hours">17</span> <span class="m2-txt2">Hours</span> </div>
      <span class="l1-txt2 p-b-22 respon2">:</span>
      <div class="flex-col-c-m wsize2 m-b-20"> <span class="l1-txt2 p-b-4 minutes">50</span> <span class="m2-txt2">Minutes</span> </div>
      <span class="l1-txt2 p-b-22">:</span>
      <div class="flex-col-c-m wsize2 m-b-20"> <span class="l1-txt2 p-b-4 seconds">39</span> <span class="m2-txt2">Seconds</span> </div>
    </div>
    <p class="txt-center p-t-30" style="color:#fff;bottom:0;">Powered by <img src="img/optimize-logolt.png" width="100"></p>
  </div>
</div>
<!-- /container-fluid -->

<div class="cd-overlay-nav"> <span></span> </div>
<!-- /cd-overlay-nav -->

<div class="cd-overlay-content"> <span></span> </div>
<!-- /cd-overlay-content --> 

<!--<a href="#0" class="cd-nav-trigger">Menu<span class="cd-icon"></span></a>--> 
<!-- /menu button --> 

<!-- Modal terms -->
<!-- /.modal --> 
@endsection
