@extends('layouts.app')

@section('content')
<div id="preloader">
  <div data-loader="circle-side"></div>
</div>
<!-- /Preload -->

<div id="loader_form">
  <div data-loader="circle-side-2"></div>
</div>
<!-- /loader_form -->

<!-- /menu -->

<div class="container-fluid full-height">
  <div class="row row-height">
    <!-- /content-left -->
    <div class="col-lg-6 content-right" id="start">
    <a href="index.html" id="logo"><img src="img/optimize-logo.png" alt=""></a>
      <div id="social">
          <ul>
            <li><a href="#0"><i class="icon-facebook"></i></a></li>
            <li><a href="#0"><i class="icon-twitter"></i></a></li>
            <li><a href="#0"><i class="icon-instagram"></i></a></li>
            <li><a href="#0"><i class="icon-linkedin"></i></a></li>
          </ul>
        </div>
        <!-- /social -->
      <div id="wizard_container">
        <div id="top-wizard">
          <div id="progressbar"></div>
        </div>
        <!-- /top-wizard -->
        <form id="wrapped" method="POST">
          <input id="website" name="website" type="text" value="">
          <!-- Leave for security protection, read docs for details -->
          <div id="middle-wizard">
            <div class="step">
              <h3 class="main_question">Forgot Password</h3>
              <div class="form-group">
                <input class="form-control required" type="password" id="password1" name="password1" placeholder="Password" onchange="getVals(this, 'password');">
              </div>
              <div class="form-group">
                <input class="form-control required" type="password" id="password2" name="password2" placeholder="Confirm Password">
              </div>
            </div>
            <!-- /step-->
          </div>
          <!-- /middle-wizard -->
          <div id="bottom-wizard">
            <button type="button" name="forward" class="forward" style=" display:inline-block">Submit</button>
          </div>
          <!-- /bottom-wizard -->
        </form>
      </div>
      <!-- /Wizard container --> 
    </div>
    <!-- /content-right-->
    <div class="col-lg-6 content-left">
      <div class="content-left-wrapper"> 
      
        <div>
          <figure><img src="img/info_graphic_2.svg" alt="" class="img-fluid"></figure>
          <h2>Registration Wizard</h2>
          <p>Tation argumentum et usu, dicit viderer evertitur te has. Eu dictas concludaturque usu, facete detracto patrioque an per, lucilius pertinacia eu vel. Adhuc invidunt duo ex. Eu tantas dolorum ullamcorper qui.</p>
          <a href="#0" class="btn_1 rounded">Purchase this template</a> <a href="#start" class="btn_1 rounded mobile_btn">Start Now!</a> </div>
        <div class="copy">© 2020 Optimize</div>
      </div>
      <!-- /content-left-wrapper --> 
    </div>
  </div>
  <!-- /row--> 
</div>
<!-- /container-fluid -->

<div class="cd-overlay-nav"> <span></span> </div>
<!-- /cd-overlay-nav -->

<div class="cd-overlay-content"> <span></span> </div>
<!-- /cd-overlay-content --> 
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
