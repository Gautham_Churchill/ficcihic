@extends('layouts.app')

@section('content')
<body class="forgot-bg">
<div id="preloader">
  <div data-loader="circle-side"></div>
</div>
<!-- /Preload -->

<div id="loader_form">
  <div data-loader="circle-side-2"></div>
</div>
<!-- /loader_form -->
<!-- /menu -->
<div class="container-fluid full-height">
  <div class="row row-height">
    <!-- /content-left -->
    <div class="col-lg-12 content-right" id="start">
    <!---->
    
    <!-- /social -->
      <div id="wizard_container">
      <a href="/" id="logo"><img src="img/optimize-logolt.png" alt=""></a>
      
        <div id="top-wizard">
          <div id="progressbar"></div>
        </div>
        <!-- /top-wizard -->
        <form id="wrapped-x" method="POST" action="{{url('/forgot-code')}}" class="form_bg">
          @csrf
          <input id="website" name="website" type="text" value="">
          <!-- Leave for security protection, read docs for details -->
          <div id="middle-wizard">
            <div class="step">

              @if(Session::has('flash_message_error'))
        <div class="alert alert-sm alert-danger alert-block" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{!! session('flash_message_error') !!}</strong>
        </div>
        @endif

        @if(Session::has('flash_message_success'))
        <div class="alert alert-sm alert-success alert-block" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{!! session('flash_message_success') !!}</strong>
        </div>
        @endif

              <h3 class="main_question">Forgot Code</h3>
              <div class="form-group">
                <input type="text" name="email" class="form-control required" placeholder="Email id" onchange="getVals(this, 'user_name');">
              </div>
            </div>
            <!-- /step-->
          </div>
          <!-- /middle-wizard -->
          <div id="bottom-wizard" style="text-align:center;">
           <!--  <button type="button" name="forward" class="forward" style=" display:inline-block">Submit</button> -->

           <input type="submit" name="submit" value="submit" class="btn btn-primary">
          </div>
          <!-- /bottom-wizard -->
        </form>
      </div>
      <!-- /Wizard container --> 
    </div>
    <!-- /content-right-->
    
    
     
  </div>
  <!-- /row--> 
</div>
<!-- /container-fluid -->

<div class="cd-overlay-nav"> <span></span> </div>
<!-- /cd-overlay-nav -->

<div class="cd-overlay-content"> <span></span> </div>
<!-- /cd-overlay-content --> 

<!--<a href="#0" class="cd-nav-trigger">Menu<span class="cd-icon"></span></a>--> 
<!-- /menu button --> 

<!-- Modal terms -->
<!-- /.modal --> 
@endsection
