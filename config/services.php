<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
    'client_id' => '1086128974925-ebj49kkem8mhg4jm83p4gbil1d2vq8ep.apps.googleusercontent.com',
    'client_secret' => 'ECuFSPvEsHO9X8-AjaSY03HK',
    'redirect' => 'http://127.0.0.1:8000/login/google/callback',
    ],

    'facebook' => [
    'client_id' => '736722550228105',
    'client_secret' => '80f8752fe75ad6ad2c896d81b283a386',
    'redirect' => 'http://127.0.0.1:8000/login/facebook/callback',
    ],

    'twitter' => [
    'client_id' => '749910045824915',
    'client_secret' => '48211409b7c310b487c4a20a3d699cf1',
    'redirect' => 'http://127.0.0.1:8000/login/facebook/callback',
    ],

     'linkedin' => [
    'client_id' => '77faxt4499dlpw',
    'client_secret' => 'zx4bqI94YyTZesB8',
    'redirect' => 'http://127.0.0.1:8000/login/linkedin/callback',
    ],



];
