<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
           $table->increments('id');
            $table->String('first_name')->nullable();
            $table->String('last_name')->nullable();
            $table->String('desgination')->nullable();
            $table->string('email',150)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->String('mobile')->nullable();
            $table->String('linkedin_link')->nullable();
            $table->String('twitter_link')->nullable();
            $table->String('image')->nullable();
            $table->String('terms')->nullable();
            $table->String('name_of_organisation')->nullable();
            $table->String('address')->nullable();
            $table->String('phone')->nullable();
            $table->String('ficci_member')->nullable();
            $table->String('ficci_no')->nullable();
            $table->String('gst')->nullable();
            $table->String('gst_no')->nullable();
            $table->enum('user_status', array('0','1','2'))->nullable();


            $table->String('google_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
